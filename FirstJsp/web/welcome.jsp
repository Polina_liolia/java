<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Wolcome JSP Page</title>
    </head>
    <body>
        <h1>Welcome! </h1>
        <%
            String name = request.getParameter("name");
            if(name == null || name.length() == 0)
            {
                %>
                Hello world!
                <%
            }
            else
            {
                %>
                Hello, 
                <%=name%>
                <%
            }
        %>
    </body>
</html>
