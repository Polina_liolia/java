package org.itstep.bean;

public class NameHandler {
    private String name;
    
    public NameHandler (){
        name = "NoName";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}