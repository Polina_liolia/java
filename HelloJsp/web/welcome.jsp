<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <jsp:useBean id="mybean" scope="session" class="org.itstep.bean.NameHandler"/>
    <jsp:setProperty name="mybean" property="name" />
    <body>
        <%
            for(int i=0; i < 3; i++)
        {
            %>
            <h1>Привет, <jsp:getProperty name="mybean" property="name"/></h1>
            <%
        }
        %>
        
    </body>
</html>
