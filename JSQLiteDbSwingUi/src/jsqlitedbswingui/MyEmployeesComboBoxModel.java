package jsqlitedbswingui;


import javax.swing.DefaultComboBoxModel;

class MyEmployeesComboBoxModel extends DefaultComboBoxModel<Employees> {

    public MyEmployeesComboBoxModel(Employees[] items) {
        super(items);
    }

    @Override
    public Employees getSelectedItem() {
        Employees selectedPerson = (Employees) super.getSelectedItem();
        //do something with this job before returning...
        return selectedPerson;
    }
}
