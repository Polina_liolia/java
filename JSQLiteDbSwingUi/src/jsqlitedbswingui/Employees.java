package jsqlitedbswingui;


public class Employees {
    private int employeesId;
    private String employees_Name;
    private int idCode;

    public Employees(int employeesId, String employees_Name, int idCode) {
        this.employeesId = employeesId;
        this.employees_Name = employees_Name;
        this.idCode = idCode;
    }

    public Employees() {
    }

    public int getEmployeesId() {
        return employeesId;
    }

    public void setEmployeesId(int employeesId) {
        this.employeesId = employeesId;
    }

    public void setEmployees_Name(String employees_Name) {
        this.employees_Name = employees_Name;
    }

    public void setIdCode(int idCode) {
        this.idCode = idCode;
    }

    public String getEmployees_Name() {
        return employees_Name;
    }

    public int getIdCode() {
        return idCode;
    }

    @Override
    public String toString() {
        return employeesId + " " + employees_Name;
    }
    
    
    
}
