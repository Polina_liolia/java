package jsqlitedbswingui;

import javax.swing.DefaultComboBoxModel;

public class DepartmentsComboBoxModel  extends DefaultComboBoxModel<Department>  {

    public DepartmentsComboBoxModel(Department[] es) {
        super(es);
    }
    
    @Override
    public Department getSelectedItem() {
        Department selectedPerson = (Department) super.getSelectedItem();
        //do something with this job before returning...
        return selectedPerson;
    }

}
