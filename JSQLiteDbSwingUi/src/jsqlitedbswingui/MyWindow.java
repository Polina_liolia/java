package jsqlitedbswingui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.HeadlessException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JFrame;

public class MyWindow extends JFrame {

    public MyWindow() {
        super("SQLite connection test");
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));

        //seed
        List<Department> departments = new ArrayList<Department>();
        departments.add(new Department(1, "Marketing"));
        departments.add(new Department(2, "Sales"));
        departments.add(new Department(3, "Development"));

        //list to array
        Department[] deptsArr = new Department[departments.size()];
        departments.toArray(deptsArr);

        //combo box
        DepartmentsComboBoxModel deptCbModel = new DepartmentsComboBoxModel(deptsArr);
        final JComboBox<Department> departmentsList = new JComboBox<Department>(deptCbModel);
        departmentsList.setForeground(Color.BLUE);
        departmentsList.setFont(new Font("Arial", Font.BOLD, 14));
        departmentsList.setMaximumRowCount(10);

        // make the combo box editable so we can add new item when needed
        departmentsList.setEditable(true);
        //add on window
        add(departmentsList);
        
        //seed
        List<Employees> employees = new ArrayList<Employees>();
        employees.add(new Employees(1, "Petia", 1));
        employees.add(new Employees(2, "Yura", 1));
        employees.add(new Employees(3, "Sasha", 2));
        employees.add(new Employees(4, "Kolia", 3));
        employees.add(new Employees(5, "Vasia", 3));
        employees.add(new Employees(6, "Misha", 2));
        

        //list to array
        Employees[] emplArr = new Employees[employees.size()];
        employees.toArray(emplArr);

        //combo box
        MyEmployeesComboBoxModel emplCbModel = new MyEmployeesComboBoxModel(emplArr);
        final JComboBox<Employees> employeesList = new JComboBox<Employees>(emplCbModel);
        employeesList.setForeground(Color.BLUE);
        employeesList.setFont(new Font("Arial", Font.BOLD, 14));
        employeesList.setMaximumRowCount(10);

        // make the combo box editable so we can add new item when needed
        employeesList.setEditable(true);
        //add on window
        add(employeesList);
        
        
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

    }

}
