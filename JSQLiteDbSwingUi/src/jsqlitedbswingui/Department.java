package jsqlitedbswingui;


public class Department {
    private int departmentsID;
    private String name;

    public void setDepartmentsID(int departmentsID, String name) {
        this.departmentsID = departmentsID;
        this.name = name;
    }

   
    public void setName(String name) {
        this.name = name;
    }

    public int getDepartmentsID() {
        return departmentsID;
    }

    public String getName() {
        return name;
    }

    public Department(int departmentsID, String name) {
        this.departmentsID = departmentsID;
        this.name = name;
    }

    public Department() {
    }
    
     @Override
    public String toString() {
        return departmentsID + " " + name;
    }

}
