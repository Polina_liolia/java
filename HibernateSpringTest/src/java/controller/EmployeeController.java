/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.dao.EmployeeDAO;
import model.pojo.Employee;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class EmployeeController implements Controller {
     
    @Override
    public ModelAndView handleRequest(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        
        ModelAndView mv = new ModelAndView("employee");//employee.jsp
        
        try {
            List<Employee> lst = EmployeeDAO.getDS();
            mv.addObject("employees", lst);
            //employees - это имя коллекции внутри View - для доступа к коллекции из jsp
        } catch (Exception e) {
        }
       
       return mv;
    }
    
}