/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.ArrayList;
import java.util.List;
import model.pojo.Employee;
import model.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class EmployeeDAO {
    public static List<Employee> getDS()
    {
       List<Employee> lst;
       EmployeeDAO worker = new EmployeeDAO();
       Session session = null;
        try {
            
            session = worker.openSession();
            String hql = "from Employee";
            Query query = session.createQuery(hql);
            lst = query.list();
        } catch (Exception e) {
            lst = new ArrayList<>();
        }
        finally
        {
            worker.closeSession(session);
        }
           return lst;
    }
//
private ServiceRegistry serviceRegistry;
//окрытие сессии
 public Session openSession() throws HibernateException {
        // loads configuration and mappings
        Configuration configuration = new Configuration().configure();
        ServiceRegistryBuilder registry = new ServiceRegistryBuilder();
        registry.applySettings(configuration.getProperties());
        serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        // builds a session factory from the service registry
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        // obtains the session
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        return session;
    }
//закрытие сессии
 public void closeSession(Session session) throws HibernateException {
        session.getTransaction().commit();
        session.close();
        //отдал сборщику мусор - пометил на удаление
        StandardServiceRegistryBuilder.destroy(serviceRegistry);
    }
//
}

