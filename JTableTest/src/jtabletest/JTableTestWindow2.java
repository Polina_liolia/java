package jtabletest;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

class JTableTestWindow2 extends JFrame {

    JTable tblString;
    List<Student> students;

    public JTableTestWindow2() {
        super("JTable test 2");
        setSize(400, 400);

        //матрица значений
        List<Student> students = new ArrayList();
        students.add(new Student(1, "Иванов", Student.Grade.A));
        students.add(new Student(1, "Петров", Student.Grade.B));
        students.add(new Student(1, "Сидоров", Student.Grade.C));

        //описание и создание модели данных
        StudentTableModel model = new StudentTableModel(students); //заголовки столбцов

        tblString = new JTable();

        tblString = new JTable(model);

        setLayout(
                new BorderLayout());
        add(
                new JScrollPane(tblString), BorderLayout.CENTER);

        tblString.setDefaultRenderer(Student.Grade.class,
                 new MyGradeRenderer()
        );
        // pack(); //подгоняет все размеры под контент

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
