package jtabletest;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class JTableTestWindow extends JFrame {

    JTable tblString;

    public JTableTestWindow() {
        super("JTable test 1");
        setSize(400, 400);

        //описание столбцов
        String[] columns = new String[]{
            "Id", "Name", "H.Rate", "Part Time"
        };

        //описание типов столбцов
        Class[] columnsType = new Class[]{
            Integer.class, String.class, Double.class, Boolean.class //по принципу typeof
        };

        //матрица значений
        Object[][] data = new Object[][]{
            {1, "Петров", 40.0, false},
            {2, "Иванов", 70.0, true},
            {3, "Сидоров", 60.0, false}
        };

        //описание и создание модели данных
        DefaultTableModel model = new DefaultTableModel(data, //данные
                columns) //заголовки столбцов
        {
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return column == 3 ? true : false;
            }
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                return columnsType[columnIndex];
            }
        };
        
        tblString = new JTable(model);
        setLayout(new BorderLayout());
        add(new JScrollPane(tblString), BorderLayout.CENTER);
        
        tblString.setDefaultRenderer(Boolean.class, new MyRenderer());
       // pack(); //подгоняет все размеры под контент
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
