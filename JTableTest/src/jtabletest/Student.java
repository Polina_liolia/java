package jtabletest;


public class Student {

    Student(int id, String name, Grade grade) {
        this.id = id;
        this.name = name;
        this.grade = grade;
    }
    public static enum Grade
    {
        A, B, C;
    }
    
    private int id;
    private String name;
    private Grade grade;
   
    //setters, getters, etc.

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Grade getGrade() {
        return grade;
    }
}
