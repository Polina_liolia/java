package jtabletest;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class MyGradeRenderer extends JLabel implements TableCellRenderer {

    public MyGradeRenderer() {
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable jtable,
            Object value,
            boolean isSelected,
            boolean isFicus,
            int row,
            int column) {
        Student.Grade grade = (Student.Grade) value;
        super.setHorizontalAlignment(CENTER);
        if (grade == Student.Grade.A) {
            super.setBackground(Color.GREEN);
            super.setText("A");
        } else if (grade == Student.Grade.B) {
            super.setBackground(Color.BLUE);
            super.setText("B");
        } else if (grade == Student.Grade.C) {
            super.setBackground(Color.RED);
            super.setText("C");
        }
        return this;
    }

}
