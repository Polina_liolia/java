package jtabletest;

import java.util.List;
import javax.swing.table.AbstractTableModel;

class StudentTableModel extends AbstractTableModel {

    List<Student> students;

    //описание столбцов
    String[] columns = new String[]{
        "Id", "Name", "Grade"
    };

    //описание типов столбцов
    Class[] columnsType = new Class[]{
        Integer.class, String.class, Student.Grade.class
    };

    public StudentTableModel(List<Student> students) {
        this.students = students;
    }

    @Override
    public int getRowCount() {
        return students.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int row, int column) {
        Student curRow = students.get(row);
        return 0 == column ? curRow.getId() : 1 == column ? curRow.getName() : 2 == column ? curRow.getGrade() : null;

    }

    @Override
    public String getColumnName(int column) {
        return columns[column];
    }
    
    @Override
    public Class<?> getColumnClass(int column)
    {
        return columnsType[column];
    }

}
