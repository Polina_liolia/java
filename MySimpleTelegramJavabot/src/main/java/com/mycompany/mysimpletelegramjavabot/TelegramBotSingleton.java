package com.mycompany.mysimpletelegramjavabot;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

public final class TelegramBotSingleton extends TelegramLongPollingBot {
    private static TelegramBotSingleton instance;
    
    private TelegramBotSingleton(){}
    
    public static synchronized TelegramBotSingleton getInstance(){
        if(instance == null){
            instance = new TelegramBotSingleton();
        }
        return instance;
    }

    @Override
    public String getBotToken() {
         return "457256850:AAFlXJqWVebYQko5ZP2orbq_KwutPx6lEqA";
    }

    @Override
    public void onUpdateReceived(Update update) {
        long chat_id = -1;
       // We check if the update has a message and the message has text
        if (update.hasMessage() && update.getMessage().hasText()) {
            //String message_text = update.getMessage().getText();
            chat_id = update.getMessage().getChatId();
            Message message = update.getMessage();
            String call_data = "";
            if (update.hasCallbackQuery()) {
                call_data = update.getCallbackQuery().getData();
            }
            StateMashine.DefineState(chat_id, message, call_data);
        }
        else if (update.hasCallbackQuery()) {
            // Set variables
            String call_data = update.getCallbackQuery().getData();
            long message_id = update.getCallbackQuery().getMessage().getMessageId();
            chat_id = update.getCallbackQuery().getMessage().getChatId();
            StateMashine.DefineState(chat_id, null, call_data);
        }
    }
    

    @Override
    public String getBotUsername() {
        return "SimpleTellegramTest03_bot";
    }

    /*
     * Метод для настройки сообщения и его отправки.
     * @param chatId id чата
     * @param s Строка, которую необходимот отправить в качестве сообщения.
     */
    public synchronized void sendMsg(long chatId, String s, InlineKeyboardMarkup replyMarkup) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(s);
        if(replyMarkup != null)
            sendMessage.setReplyMarkup(replyMarkup);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            //log.log(Level.SEVERE, "Exception: ", e.toString());
        }
    }
}
