package com.mycompany.mysimpletelegramjavabot;

public enum BotEvents {
    Start,
    StartAccepted,
    StartRejected,
    Scissors,
    Stone,
    Paper,
    Undefined
}
