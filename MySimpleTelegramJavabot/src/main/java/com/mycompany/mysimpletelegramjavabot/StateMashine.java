package com.mycompany.mysimpletelegramjavabot;

import com.mycompany.mysimpletelegramjavabot.States.StateData;
import org.telegram.telegrambots.api.objects.Message;

public class StateMashine {
 private static UserMessage userMessage;
        private StateMashine()
        { }
        
        public static StateMashine Create()
        {
            return new StateMashine();
        }

        private static BotEvents generateEvent(String msgText)
        {
            switch(msgText)
            {
                case "/start": return BotEvents.Start;
                case "/stone": return BotEvents.Stone;
                case "/scissors": return BotEvents.Scissors;
                case "/paper": return BotEvents.Paper;
                case "btnYes": return BotEvents.StartAccepted;
                case "btnNo": return BotEvents.StartRejected;
                default: return BotEvents.Undefined;
            }
        }

    
    public static void DefineState(long chatId, Message msg, String callbackQueryData)
        {
            if (userMessage == null)
                userMessage = new UserMessage(msg);
            else
                userMessage.msg = msg;
            String msgArg = ("".equals(callbackQueryData) && msg != null) ? msg.getText() : callbackQueryData;
            BotEvents currentEvent = generateEvent(msgArg);
            
            StateData reply = userMessage.FindOut(currentEvent);
            TelegramBotSingleton.getInstance().sendMsg(chatId, reply.getAnswer(), reply.getReplyMarkup());
        }


}
