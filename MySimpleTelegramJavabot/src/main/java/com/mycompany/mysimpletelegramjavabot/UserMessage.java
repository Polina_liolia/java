package com.mycompany.mysimpletelegramjavabot;

import com.mycompany.mysimpletelegramjavabot.States.NeutralState;
import com.mycompany.mysimpletelegramjavabot.States.State;
import com.mycompany.mysimpletelegramjavabot.States.StateData;
import org.telegram.telegrambots.api.objects.Message;

public class UserMessage {

    public Message msg;
    public State state;

    public UserMessage(Message msg) {
        this.state = new NeutralState();
        this.msg = msg;
    }

    public StateData FindOut(BotEvents botEvent) {
        return state.HandleEvent(this, botEvent);
    }

    public Message getMsg() {
        return msg;
    }

    public void setMsg(Message msg) {
        this.msg = msg;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

}
