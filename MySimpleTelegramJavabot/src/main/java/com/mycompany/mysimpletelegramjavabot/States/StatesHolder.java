package com.mycompany.mysimpletelegramjavabot.States;

import java.util.HashMap;
import java.util.Map;

public class StatesHolder {

    private static Map<String, State> states;

    static {
        states = new HashMap<String, State>();
        states.put("neutral", new NeutralState());
        states.put("startRequested", new StartRequestedState());
        states.put("started", new GameStartedState());
        states.put("rejected", new GameRejectedState());
    }
     

       public static State getState(String name){
            return states.get(name);
    }
}
