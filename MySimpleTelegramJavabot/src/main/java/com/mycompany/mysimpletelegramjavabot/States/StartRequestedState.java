package com.mycompany.mysimpletelegramjavabot.States;

import com.mycompany.mysimpletelegramjavabot.BotEvents;
import com.mycompany.mysimpletelegramjavabot.UserMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;

class StartRequestedState extends State {

    protected StateData ChangeState(UserMessage message, BotEvents botEvent)
        {
            String answer = "";
            InlineKeyboardMarkup replyMarkup = null;
            switch(botEvent)
            {
                case StartAccepted:
                    answer = "Поехали! Сделайте свой выбор: /stone, /scissors, /paper"; 
                    message.setState(StatesHolder.getState("started"));
                    break;
                case StartRejected:
                    answer = "Поиграем в другой раз...";
                    message.setState(StatesHolder.getState("rejected"));
                    break;
                default:
                    answer = "Вы еще не приняли решение, играть или нет. Нажмите на соответствующую кнопку.";
                    break;
            }
            return new StateData(answer, replyMarkup);
        }

}
