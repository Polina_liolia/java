package com.mycompany.mysimpletelegramjavabot.States;

import com.mycompany.mysimpletelegramjavabot.BotEvents;
import com.mycompany.mysimpletelegramjavabot.UserMessage;
import java.util.ArrayList;
import java.util.List;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

class GameStartedState extends State {

    @Override
    protected StateData ChangeState(UserMessage message, BotEvents botEvent) {
        String answer = "";
        message.setState(StatesHolder.getState("started"));
        InlineKeyboardMarkup replyMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();
        rowInline.add(new InlineKeyboardButton().setText("Да").setCallbackData("btnYes"));
        rowInline.add(new InlineKeyboardButton().setText("Нет").setCallbackData("btnNo"));
        // Set the keyboard to the markup
        rowsInline.add(rowInline);
        // Add it to the message
        replyMarkup.setKeyboard(rowsInline);
        message.setState(StatesHolder.getState("startRequested"));
        int botSignNumber =  1 + (int) (Math.random() * 3); //generating random number for bot
        BotEvents botSign = botSignNumber == 1 ? BotEvents.Scissors : 
                botSignNumber == 2 ? BotEvents.Stone : BotEvents.Paper;
        switch (botEvent) {
            case Scissors:
                switch(botSign){
                    case Scissors: answer = "У меня тоже ножницы - ничья! Сыграем еще?"; break;
                    case Stone: answer = "У меня камень - вы проиграли! Сыграем еще?"; break;
                    case Paper: answer = "У меня бумага - вы выиграли! Сыграем еще?"; break;
                }
                break;
            case Stone:
                switch(botSign){
                    case Stone: answer = "У меня тоже камень - ничья! Сыграем еще?"; break;
                    case Paper: answer = "У меня бумага - вы проиграли! Сыграем еще?"; break;
                    case Scissors: answer = "У меня ножницы - вы выиграли! Сыграем еще?"; break;
                }
                break;
            case Paper:
                switch(botSign){
                    case Paper: answer = "У меня тоже бумага - ничья! Сыграем еще?"; break;
                    case Scissors: answer = "У меня ножницы - вы проиграли! Сыграем еще?"; break;
                    case Stone: answer = "У меня камень - вы выиграли! Сыграем еще?"; break;
                }
                break;
            default:
                answer = "Сделайте свой выбор: /stone, /scissors, /paper";
                replyMarkup = null;
                message.setState(StatesHolder.getState("started"));//as GameStartedState;
                break;
        }
        return new StateData(answer, replyMarkup);
    }

}
