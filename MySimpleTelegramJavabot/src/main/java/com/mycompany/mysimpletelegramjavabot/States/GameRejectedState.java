package com.mycompany.mysimpletelegramjavabot.States;

import com.mycompany.mysimpletelegramjavabot.BotEvents;
import com.mycompany.mysimpletelegramjavabot.UserMessage;
import java.util.ArrayList;
import java.util.List;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

class GameRejectedState extends State {

    @Override
    protected StateData ChangeState(UserMessage message, BotEvents botEvent) {
        String answer = "Вы передумали и хотите играть?";
        InlineKeyboardMarkup replyMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();
        rowInline.add(new InlineKeyboardButton().setText("Да").setCallbackData("btnYes"));
        rowInline.add(new InlineKeyboardButton().setText("Нет").setCallbackData("btnNo"));
        // Set the keyboard to the markup
        rowsInline.add(rowInline);
        // Add it to the message
        replyMarkup.setKeyboard(rowsInline);
        message.setState(StatesHolder.getState("startRequested"));
        return new StateData(answer, replyMarkup);
    }

}
