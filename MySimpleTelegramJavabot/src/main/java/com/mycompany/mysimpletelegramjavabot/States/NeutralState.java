package com.mycompany.mysimpletelegramjavabot.States;

import com.mycompany.mysimpletelegramjavabot.BotEvents;
import com.mycompany.mysimpletelegramjavabot.TelegramBotSingleton;
import com.mycompany.mysimpletelegramjavabot.UserMessage;
import java.util.ArrayList;
import java.util.List;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

public class NeutralState extends State {

    @Override
    protected StateData ChangeState(UserMessage message, BotEvents botEvent) {
        StringBuilder sbAnswer = new StringBuilder();
        sbAnswer.append("Здравствуйте, я ");
        sbAnswer.append(TelegramBotSingleton.getInstance().getBotUsername());
        InlineKeyboardMarkup replyMarkup = null;
        switch (botEvent) {
            case Start: {
                //TODO: get sender name
//                        String firstName = message.From.FirstName;
//                        String lastName = message.Msg.From.LastName;
//                        String userName = message.Msg.From.Username;
//                        sbAnswer.append(firstName);
//                        sbAnswer.append(" ");
//                        sbAnswer.append(lastName);

                sbAnswer.append(", сыграем в игру 'Камень-ножницы-бумага'?");
                sbAnswer.append("/stone - камень");
                sbAnswer.append("/scissors - ножницы");
                sbAnswer.append("/paper - бумага");
                message.setState(StatesHolder.getState("startRequested"));

                replyMarkup = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
                List<InlineKeyboardButton> rowInline = new ArrayList<>();
                rowInline.add(new InlineKeyboardButton().setText("Да").setCallbackData("btnYes"));
                rowInline.add(new InlineKeyboardButton().setText("Нет").setCallbackData("btnNo"));
                // Set the keyboard to the markup
                rowsInline.add(rowInline);
                // Add it to the message
                replyMarkup.setKeyboard(rowsInline);
                break;
            }
            default: {
                sbAnswer.append("Чтобы начать, напишите мне /start.");
                break;
            }
        }
        return new StateData(sbAnswer.toString(), replyMarkup);
    }
}
