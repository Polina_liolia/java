package com.mycompany.mysimpletelegramjavabot.States;

import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;

public class StateData {
    private String answer;
    private InlineKeyboardMarkup replyMarkup;

    public StateData(String answer, InlineKeyboardMarkup replyMarkup) {
        this.answer = answer;
        this.replyMarkup = replyMarkup;
    }

    private StateData() {
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public InlineKeyboardMarkup getReplyMarkup() {
        return replyMarkup;
    }

    public void setReplyMarkup(InlineKeyboardMarkup replyMarkup) {
        this.replyMarkup = replyMarkup;
    }
    
    
}
