package com.mycompany.mysimpletelegramjavabot.States;

import com.mycompany.mysimpletelegramjavabot.BotEvents;
import com.mycompany.mysimpletelegramjavabot.UserMessage;

 public abstract class State
    {
        protected abstract StateData ChangeState(UserMessage message, BotEvents botEvent);

        public StateData HandleEvent(UserMessage message, BotEvents botEvent)
        {
            return ChangeState(message, botEvent);
        }
    }
