package jcombobox;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

public class JComboBoxSimpleTestWindow extends JFrame {

    private JButton buttonSelect = new JButton("Select");
    private JButton buttonRemove = new JButton("Remove");

    public JComboBoxSimpleTestWindow() {
        super("Swing JComboBox простой тест");
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));

        String[] bookTitles = new String[]{"Гарри Поттер и философский камень",
            "Гарри Поттер и тайная комната", "Гарри Поттер и узник Азкабана"};

        // create a combo box with items specified in the String array:
        final JComboBox<String> bookList = new JComboBox<String>(bookTitles);

        // add more books
        bookList.addItem("Гарри Поттер и Кубок огня");
        bookList.addItem("Гарри Поттер и Орден Феникса");
        bookList.addItem("Гарри Поттер и Принц-полукровка");
        bookList.addItem("Гарри Поттер и Дары Смерти часть 1");
        bookList.addItem("Гарри Поттер и Дары Смерти часть 2");

        // customize some appearance:
        bookList.setForeground(Color.BLUE);
        bookList.setFont(new Font("Arial", Font.BOLD, 14));
        bookList.setMaximumRowCount(10);

        // компонент становится редактируемым
        bookList.setEditable(true);
        
        // добавляем реакцию на ввод данных
        //теперь, если мы введем новое (которого не было) или изменим существубщее значение
        //и нажмем Enter то выполнится код ниже
        bookList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                JComboBox<String> combo = (JComboBox<String>) event.getSource();
                String selectedBook = (String) combo.getSelectedItem();
                DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) combo.getModel();
                int selectedIndex = model.getIndexOf(selectedBook);
                if (selectedIndex < 0) {
                    // если выбранной книги не было,
                    // то добавляем ее в комбобокс
                    model.addElement(selectedBook);
                }
            }
        });
        
        // реакция на кнопку button Select
        buttonSelect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String selectedBook = (String) bookList.getSelectedItem();
                JOptionPane.showMessageDialog(JComboBoxSimpleTestWindow.this,
                        "You selected the book: " + selectedBook);
            }
        });
        
        // реакция на кнопку the button Remove
        buttonRemove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String selectedBook = (String) bookList.getSelectedItem();
                bookList.removeItem(selectedBook);
            }
        });

        // add components to this frame
        add(bookList);
        add(buttonSelect);
        add(buttonRemove);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

}
