package jcombobox;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;


public class JComboBoxUserClassTestWindow extends JFrame {

    private JButton buttonSelect = new JButton("Select");
    private JButton buttonRemove = new JButton("Remove");

    public JComboBoxUserClassTestWindow() {
        super("Swing JComboBox тест отображения списка объектов собственного класса");
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        List<Employee> listEmployees = new ArrayList<Employee>();
        listEmployees.add(new Employee("Tom", 45, 80000));
        listEmployees.add(new Employee("Sam", 56, 75000));
        listEmployees.add(new Employee("Alex", 30, 120000));
        listEmployees.add(new Employee("Peter", 25, 60000));
        Employee[] employees = new Employee[listEmployees.size()];
        listEmployees.toArray(employees);
        MyComboBoxModel myModel = new MyComboBoxModel(employees);
        final JComboBox<Employee> employeeList = new JComboBox<Employee>(myModel);
        employeeList.setForeground(Color.BLUE);
        employeeList.setFont(new Font("Arial", Font.BOLD, 14));
        employeeList.setMaximumRowCount(10);

        // make the combo box editable so we can add new item when needed
        employeeList.setEditable(true);

        // add an event listener for the combo box
        employeeList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                JComboBox<Employee> combo = (JComboBox<Employee>) event.getSource();
                Employee selectedEmployee = (Employee) combo.getSelectedItem();
                DefaultComboBoxModel<Employee> model = (DefaultComboBoxModel<Employee>) combo.getModel();
                int selectedIndex = model.getIndexOf(selectedEmployee);
                if (selectedIndex < 0) {
                    // if the selected book does not exist before,
                    // add it into this combo box
                    model.addElement(selectedEmployee);
                }
            }
        });

        // add event listener for the button Select
        buttonSelect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Employee selectedEmployee = (Employee) employeeList.getSelectedItem();
                JOptionPane.showMessageDialog(JComboBoxUserClassTestWindow.this,
                        "You selected Employee: " + selectedEmployee);
            }
        });

        // add event listener for the button Remove
        buttonRemove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Employee selectedEmployee = (Employee) employeeList.getSelectedItem();
                employeeList.removeItem(selectedEmployee);
            }
        });

        // add components to this frame
        add(employeeList);
        add(buttonSelect);
        add(buttonRemove);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
}
