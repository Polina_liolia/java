package jcombobox;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class JComboBox {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new JComboBoxSimpleTestWindow().setVisible(true);
                
                new JComboBoxUserClassTestWindow().setVisible(true);
            }
        });
    }

}
