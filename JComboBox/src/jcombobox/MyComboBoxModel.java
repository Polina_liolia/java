package jcombobox;

import javax.swing.DefaultComboBoxModel;

class MyComboBoxModel extends DefaultComboBoxModel<Employee> {

    public MyComboBoxModel(Employee[] items) {
        super(items);
    }

    @Override
    public Employee getSelectedItem() {
        Employee selectedJob = (Employee) super.getSelectedItem();
        //do something with this job before returning...
        return selectedJob;
    }
}
