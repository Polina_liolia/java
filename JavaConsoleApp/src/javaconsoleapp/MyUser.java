/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaconsoleapp;

import java.util.Objects;

/**
 *
 * @author asp
 */
public class MyUser {
    private int id; //field = в java в нижнем регистре
    private String name; //для получения корректных set/get методов. В Java поле становится свойством property, если есть пара конкретных set/get

    //конструктор с параметрами для удобства
    public MyUser(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public MyUser() {
    }

    @Override
    public String toString() {
        return "MyUser{" + "id=" + id + ", name=" + name + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.id;
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MyUser other = (MyUser) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
