/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaconsoleapp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asp
 */
public class JavaConsoleApp {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            //System.out.println("Введите N:");
            //double d = GetDoubleFromBuffer();
            //System.out.println("double value is " + d);
            
            //ScannerTest();
            
            //File Scanner
            FileScannerTest();
            
            int[] array = new int[5];
            for(int i = 0; i < array.length; i++){
                array[i] = i + 1;
            }
            for(double i : array) // Foreach, Foreach - внутри коллекции
                System.out.println(i);
            
            double[] array2 = new double[5];
            for(int i = 0; i < array2.length; i++){
                array2[i] = Math.random();
            }
            int myNum = 1;
            System.out.println("before myNum = " + myNum);
            System.out.println("orig.array2");
            printArray(array2);
            System.out.println("after myNum = " + myNum);
            changeValue(array2, myNum, -123);
            System.out.println("new.array2");
            printArray(array2);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JavaConsoleApp.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static void ScannerTest() {       
        Scanner in = new Scanner(System.in);
        System.out.println("Введите целое число:");
        int a = in.nextInt();//считываем целое число
        System.out.println("Вы ввели: "+ a);
        
        System.out.println("Введите целое число 0-255:");
        byte b = in.nextByte();//считываем байтовое число
        System.out.println("Вы ввели: "+ b);
        
        System.out.println("Введите строку");
        String c = in.nextLine();//считываем одну строку целиком
        System.out.println("Вы ввели: "+ c);
        
        System.out.println("Введите дробное");
        double d1 = in.nextDouble();//считываем вещественное число
        System.out.println("Вы ввели: "+ d1);
        
        System.out.println("Введите целое число:");
        long  e = in.nextLong();//считываем длинное целое число
        System.out.println("Вы ввели: "+ e);
        
        System.out.println("Введите целое число:");
        short f = in.nextShort();//считываем короткое целое число
        System.out.println("Вы ввели: "+ f);
        
        System.out.println("Введите строку с пробелами");
        String s = in.next();//считываем строку до первого пробела
        System.out.println("Вы ввели: "+ s);
    }
    
    private static void FileScannerTest() throws FileNotFoundException
    {
        try
        {
            System.out.println("Ввод строк для сохранения в файл.(Ввод до слова stop)");
            try
            {
                BufferedReader input = new BufferedReader(new  InputStreamReader(System.in));
                PrintWriter output = new PrintWriter(new FileWriter("data.txt"));
                while(true)
                {
                    String strtmp = input.readLine();
                    if (strtmp.equals("stop"))
                        break;
                    output.println(strtmp);
                }
                output.close();
            }
            catch(Exception ex)
            {
                
            }
            BufferedReader inputFile = new BufferedReader(new FileReader("data.txt"));
            String strtmp2;
            while((strtmp2=inputFile.readLine())!=null)
            {
                System.out.println(strtmp2);
            }
            inputFile.close();
            int AA =0,BB= 0;
            Random randomGenerator = new Random();
            int randomInt = randomGenerator.nextInt(100);
            boolean bb = true;
        }
        catch(IOException ex)
        {
            Logger.getLogger(JavaConsoleApp.class.getName()).log(Level.SEVERE, null, ex);
        
        }
    }

    private static double GetDoubleFromBuffer() throws NumberFormatException {
        byte[] bytes = new byte[200];
        try {
            int count = System.in.read(bytes);
        } catch (IOException ex) {
            Logger.getLogger(JavaConsoleApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        double d = Double.parseDouble(new String(bytes));
        return d;
    }
    private static void printArray(double[] array2) {
        for(double i : array2) // Foreach, Foreach - внутри коллекции
            System.out.println(i);
    }
    public static void changeValue(double[] array,
            int num,  //в C# есть ref, out, in
            //аналогов в JAVA нет, либо как строка, либо как массив
            double newValue){
        array[num] = newValue;
        num++;
    }
    public static void swp(int a, int b){
        int t = a;
        a = b;
        b = t;
    }
    public static void swp(int[] args){
        int t = args[0];
        args[0] = args[1];
        args[1] = t;
    }
}
//JDBC = ADO.NET
//Hibernate = Entity