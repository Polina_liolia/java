
package jdbc_azureconnectiontest;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JDBC_AzureConnectionTest {

  
    public static void main(String[] args) {
        try {
            Connection conn = null;
            try {
                String dbURL = "jdbc:sqlserver://azurehibernatetestsrv.database.windows.net:1433;database=azurehibernatetest;user=artimed@azurehibernatetestsrv;password=Azure$13;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";
                conn = DriverManager.getConnection(dbURL);
                if (conn != null) {
                    DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
                    System.out.println("Driver name: " + dm.getDriverName());
                    System.out.println("Driver version: " + dm.getDriverVersion());
                    System.out.println("Product name: " + dm.getDatabaseProductName());
                    System.out.println("Product version: " + dm.getDatabaseProductVersion());
                }
            } catch (SQLException ex) {
                Logger.getLogger(JDBC_AzureConnectionTest.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            Statement stmt = conn.createStatement();
            try {
                //stmt.setQueryTimeout(iTimeout);
                ResultSet rs = stmt.executeQuery("SELECT ID, NAME FROM [dbo].[TestTable]");
                try {
                    while (rs.next()) {
                        String sResult = rs.getString("NAME");
                        int nResult = rs.getInt("ID");
                        System.out.print(sResult);
                        System.out.print(" ");
                        System.out.println(nResult);
                    }
                } finally {
                    try {
                        rs.close();
                    } catch (Exception ignore) {
                    }
                }
            } finally {
                try {
                    stmt.close();
                } catch (Exception ignore) {
                }
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(JDBC_AzureConnectionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
}
