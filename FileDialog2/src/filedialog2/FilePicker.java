package filedialog2;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author asp
 */
public class FilePicker extends JPanel {

    private JLabel label;
    private JTextField textField;
    private JButton button;
    private JFileChooser fileChooser;
    private JLabel lbl_sumResult;
    private JButton btn_CalcSum;

    private int mode;
    public static final int MODE_OPEN = 1;
    public static final int MODE_SAVE = 2;

    public FilePicker() {
        InitializeComponent();
        this.fileChooser = new JFileChooser();
    }

    private void InitializeComponent() {
        this.setLayout(new GridLayout(2, 3));

        this.label = new JLabel("Pick a file");
        this.textField = new JTextField(20);
        this.button = new JButton("Browse...");
        this.button.addActionListener(
                new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                BrowseButtonActionPerformed(ae);
            }
        }
        );

        this.add(label);
        this.add(textField);
        this.add(button);

        this.btn_CalcSum = new JButton("Calculate sum");
        this.btn_CalcSum.addActionListener(
                new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                CalcSumButtonActionPerformed(ae);
            }
        }
        );
        this.add(btn_CalcSum);

        this.lbl_sumResult = new JLabel("0");
        this.add(lbl_sumResult);

    }

    private void BrowseButtonActionPerformed(ActionEvent event) {
        if (mode == MODE_OPEN) {
            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                textField.setText(fileChooser.getSelectedFile().getAbsolutePath());
            }
        } else if (mode == MODE_SAVE) {
            if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                textField.setText(fileChooser.getSelectedFile().getAbsolutePath());
            }
        }
    }

    private void CalcSumButtonActionPerformed(ActionEvent event) {
        String filePath = this.textField.getText();
        int sum = 0;
        if (filePath.equals("")) {
            showMessageDialog(null, "No file selected");
        } else {
            File file = new File(filePath);
            try {
                Scanner input = new Scanner(file);
                while (input.hasNextLine()) {
                int number = input.nextInt();
                sum += number;
            }
            input.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FilePicker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.lbl_sumResult.setText(String.valueOf(sum));
    }

    public void addFileTypeFilter(String extension, String description) {
        FileTypeFilter filter = new FileTypeFilter(extension, description);
        fileChooser.addChoosableFileFilter(filter);
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getSelectedFilePath() {
        return textField.getText();
    }

    public JFileChooser getFileChooser() {
        return this.fileChooser;
    }

}
