package filedialog2;

import java.awt.FlowLayout;
import java.awt.HeadlessException;
import javax.swing.JFileChooser;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class FileDialog2 extends JFrame {

    public FileDialog2() {
        super("Test using JFilePicker");
        InitializeComponent();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 300);
        setLocationRelativeTo(null); // center on screen
    }

    private void InitializeComponent() {
        setLayout(new FlowLayout());
        // set up a file picker component
        FilePicker filePicker = new FilePicker();
        filePicker.setMode(FilePicker.MODE_SAVE);
        filePicker.addFileTypeFilter(".jpg", "JPEG Images");
        filePicker.addFileTypeFilter(".mp4", "MPEG-4 Videos");
        filePicker.addFileTypeFilter(".dat", "Data files");
        // access JFileChooser class directly
        JFileChooser fileChooser = filePicker.getFileChooser();
        fileChooser.setCurrentDirectory(new File("D:/"));
        // add the component to the frame
        add(filePicker);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() { //для того, чтобы полностью закрывать окно (иначе зависает в TaskManager)
            @Override
            public void run() {
                new FileDialog2().setVisible(true);
            }
        });

    }

}
