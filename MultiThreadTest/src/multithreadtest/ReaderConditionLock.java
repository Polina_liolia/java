package multithreadtest;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReaderConditionLock implements Runnable{
    private String readerName;
    ReentrantLock locker;
    Condition condition;
    public ReaderConditionLock(String readerName, ReentrantLock locker, Condition condition) {
        this.readerName = readerName;
        this.locker = locker;
        this.condition = condition;
    }

    
    @Override
    public void run() {
        try {
             read(this.readerName);
        } catch (InterruptedException ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //reading from buffer:
    private void read(String threadName) throws InterruptedException{
        this.locker.lock();
        try {
            while(MultiThreadTest.buffer.length() < MultiThreadTest.bufferLength)
                condition.await(); //waiting while writer writes
            System.out.printf("Reader %s:", threadName);
            for(int i = 0; i < MultiThreadTest.bufferLength; i++)
            {
                Thread.sleep(MultiThreadTest.pause);
                System.out.print(MultiThreadTest.buffer.charAt(i));
                System.out.println();
            }
            condition.signalAll();
        } finally {
            this.locker.unlock();
        }
    }
}
