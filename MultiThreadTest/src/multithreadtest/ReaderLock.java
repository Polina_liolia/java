package multithreadtest;

import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReaderLock implements Runnable{
    private String readerName;
    ReentrantLock locker;
    public ReaderLock(String readerName, ReentrantLock locker) {
        this.readerName = readerName;
        this.locker = locker;
    }

    
    @Override
    public void run() {
        try {
             read(this.readerName);
        } catch (InterruptedException ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //reading from buffer:
    private void read(String threadName) throws InterruptedException{
        this.locker.lock();
        try {
            System.out.printf("Reader %s:", threadName);
            for(int i = 0; i < MultiThreadTest.bufferLength; i++)
            {
                Thread.sleep(MultiThreadTest.pause);
                System.out.print(MultiThreadTest.buffer.charAt(i));
                System.out.println();
            }
        } finally {
            this.locker.unlock();
        }
    }
}
