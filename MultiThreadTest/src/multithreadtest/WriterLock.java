package multithreadtest;

import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WriterLock implements Runnable{
    ReentrantLock locker;
    public WriterLock(ReentrantLock locker) {
        this.locker = locker;
    }
    
//wrinting information into buffer:
    private void write() throws InterruptedException {
        this.locker.lock();
        try {
            MultiThreadTest.buffer.setLength(0);
            System.err.print("Writer writes: ");
            
            //generating random string
            Random random = new Random();
            for (int i = 0; i < MultiThreadTest.bufferLength; i++) {
                Thread.sleep(MultiThreadTest.pause);
                char ch = (char) ('A' + random.nextInt(26));
                System.err.print(ch);
                MultiThreadTest.buffer.append(ch);
            }
            System.out.println();
            Thread.sleep(MultiThreadTest.pause);
        } finally {
            this.locker.unlock();
        }
    }

    @Override
    public void run() {
        try {
            write();
        } catch (InterruptedException ex) {
            Logger.getLogger(Writer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
