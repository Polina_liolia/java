package multithreadtest;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Writer implements Runnable {

    //wrinting information into buffer:
    private static void write() throws InterruptedException {
        MultiThreadTest.buffer.setLength(0);
        System.err.print("Writer writes: ");

        //generating random string
        Random random = new Random();
        for (int i = 0; i < MultiThreadTest.bufferLength; i++) {
            Thread.sleep(MultiThreadTest.pause);
            char ch = (char) ('A' + random.nextInt(26));
            System.err.print(ch);
            MultiThreadTest.buffer.append(ch);
        }
        System.out.println();
        Thread.sleep(MultiThreadTest.pause);
    }

    @Override
    public void run() {
        try {
            write();
        } catch (InterruptedException ex) {
            Logger.getLogger(Writer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
