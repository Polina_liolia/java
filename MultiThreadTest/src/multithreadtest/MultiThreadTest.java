package multithreadtest;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MultiThreadTest {

    public static final int iterationNumber = 3;
    public static final int readersNumber = 5;
    public static boolean[] fArr = new boolean[readersNumber];
    public static final StringBuilder buffer = new StringBuilder();
    public static final int bufferLength = 10;
    public static final int pause = 1000;

    public static void main(String[] args) throws InterruptedException {
        //1)synchronized;
//        while (true) {
//            Thread writer = new Thread(new Writer());
//            Thread reader1 = new Thread(new Reader("Thread-1"));
//            Thread reader2 = new Thread(new Reader("Thread-2"));
//            Thread reader3 = new Thread(new Reader("Thread-3"));
//
//            writer.start();
//            synchronized (writer) {
//                writer.wait();
//            }
//            reader1.start();
//            synchronized (reader1) {
//                reader1.wait();
//            }
//            reader2.start();
//            synchronized (reader2) {
//                reader2.wait();
//            }
//            reader3.start();
//            synchronized (reader3) {
//                reader3.wait();
//                reader3.notifyAll();
//            }
//        }

        //2) ReentrantLock with Condition;
//        ReentrantLock locker = new ReentrantLock();
//        Condition condition = locker.newCondition();
//        while (true) {
//            Thread writer = new Thread(new WriterConditionLock(locker, condition));
//            writer.start();
//            Thread reader1 = new Thread(new ReaderConditionLock("Thread-1", locker, condition));
//            reader1.start();
//            Thread reader2 = new Thread(new ReaderConditionLock("Thread-2", locker, condition));
//            reader2.start();
//            Thread reader3 = new Thread(new ReaderConditionLock("Thread-3", locker, condition));
//            reader3.start();
//        }
        
        //3) ReentrantLock without Condition.
        ReentrantLock locker = new ReentrantLock();
        while (true) {
            Thread writer = new Thread(new WriterLock(locker));
            writer.start();
            Thread reader1 = new Thread(new ReaderLock("Thread-1", locker));
            reader1.start();
            Thread reader2 = new Thread(new ReaderLock("Thread-2", locker));
            reader2.start();
            Thread reader3 = new Thread(new ReaderLock("Thread-3", locker));
            reader3.start();
        }

    }

}
