package multithreadtest;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Reader implements Runnable{

    private String readerName;
    public Reader(String readerName) {
        this.readerName = readerName;
    }

    
    @Override
    public void run() {
        try {
             read(this.readerName);
        } catch (InterruptedException ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //reading from buffer:
    private static void read(String threadName) throws InterruptedException{
        System.out.printf("Reader %s:", threadName);
        for(int i = 0; i < MultiThreadTest.bufferLength; i++)
        {
            Thread.sleep(MultiThreadTest.pause);
            System.out.print(MultiThreadTest.buffer.charAt(i));
            System.out.println();
        }
    }
}
