<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%--<%@ page session="false" %>--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="employee" scope="session" class="model.pojo.Employees"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add employee</title>
    </head>
    <body>
        <h2>
            Edit employee
        </h2>

        <c:url var="addAction" value="/employees/edit" ></c:url>

        <form:form action="${addAction}" commandName="employee" method="post">

            <table>
                <c:if test="${!empty employee.employeesName}">
                    <tr>
                        <td>
                            <form:label path="employeesId">
                                <spring:message text="ID"/>
                            </form:label>
                        </td>
                        <td>
                            <form:input path="employeesId" readonly="true" size="8"  disabled="true" />
                            <form:hidden path="employeesId" />
                        </td> 
                    </tr>
                </c:if>
                <tr>
                    <td>
                        <form:label path="employeesName">
                            <spring:message text="Name"/>
                        </form:label>
                    </td>
                    <td>
                        <form:input path="employeesName" />
                    </td> 
                </tr>
                <tr>
                    <td>
                        <form:label path="idCode">
                            <spring:message text="Id Code"/>
                        </form:label>
                    </td>
                    <td>
                        <form:input path="idCode" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit"
                               value="<spring:message text="Edit Employee"/>" />
                    </td>
                </tr>
            </table>	
        </form:form>
        <br>
    </body>
</html>
