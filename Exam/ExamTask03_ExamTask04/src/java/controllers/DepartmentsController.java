/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dao.DepartmentsDAO;
import model.pojo.Departments;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class DepartmentsController implements Controller {
     
    @Override
    public ModelAndView handleRequest(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        
        ModelAndView mv = new ModelAndView("departments");//departments.jsp
        
        try {
            List<Departments> lst = DepartmentsDAO.getDS();
            mv.addObject("departments", lst);
            //departments - это имя коллекции внутри View - для доступа к коллекции из jsp
        } catch (Exception e) {
        }
       
       return mv;
    }
}