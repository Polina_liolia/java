/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.dao.EmployeesDAO;
import model.pojo.Employees;
import org.springframework.http.MediaType;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class EmployeesController {//implements Controller {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "index";
    }

    @RequestMapping(value = "/employees", method = RequestMethod.GET)
    public String listEmployees(Model model) {
        try {
            List<Employees> lst = EmployeesDAO.getEmployeesList();
            model.addAttribute("employees", lst);
        } catch (Exception e) {
        }
        return "employees";

    }

    //For add and update employee both
    @RequestMapping(value = "/employees/add", method = RequestMethod.GET)
    public String addEmployee(Model model) {
        model.addAttribute("employee", new Employees());
        return "addEmployee";

    }

    //For add and update employee both
    @RequestMapping(value = "/employees/add", method = RequestMethod.POST)
    public String addEmployee(@ModelAttribute("employee") Employees empl) {
        try {
            if (empl.getEmployeesId() == null || empl.getEmployeesId() == 0) {
                //new employee, add it
                EmployeesDAO.addEmployee(empl);
            } else {
                //existing person, call update
                EmployeesDAO.updateEmployee(empl);
            }
        } catch (Exception e) {
        }
        return "redirect:/employees";
    }

    @RequestMapping(value = "/employees/edit", method = RequestMethod.GET)
    public String editEmployee(@RequestParam("id") long id, Model model) {
        try {
            Employees empl = EmployeesDAO.getEmployeeById(id);
            model.addAttribute("employee", empl);
        } catch (Exception e) {
        }
        return "editEmployee";
    }

    @RequestMapping(value = "/employees/edit", method = RequestMethod.POST)
    public String editEmployee(@ModelAttribute("employee") Employees empl) {
        try {
            EmployeesDAO.updateEmployee(empl);
        } catch (Exception e) {
        }
        return "redirect:/employees";
    }

    @RequestMapping(value = "/employees/remove", method = RequestMethod.GET)
    public String removeEmployee(@RequestParam("id") long id) {
        try {
            EmployeesDAO.removeEmployee(id);
        } catch (Exception e) {
        }
        return "redirect:/employees";
    }

}
