/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.ArrayList;
import java.util.List;
import model.pojo.Departments;
import org.hibernate.Query;
import org.hibernate.Session;

public class DepartmentsDAO extends AbstractModelDAO{
    public static List<Departments> getDS()
    {
       List<Departments> lst;
       DepartmentsDAO worker = new DepartmentsDAO();
       Session session = null;
        try {
            session = worker.openSession();
            String hql = "from Departments";
            Query query = session.createQuery(hql);
            lst = query.list();
        } catch (Exception e) {
            lst = new ArrayList<>();
        }
        finally
        {
            worker.closeSession(session);
        }
           return lst;
    }
}

