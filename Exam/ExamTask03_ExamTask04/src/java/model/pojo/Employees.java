package model.pojo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Employees implements Serializable {

    private Long employeesId;
    private String employeesName;
    private Integer idCode;
    private Set<Departments> departments = new HashSet<>();

    public Employees() {
    }

    public Employees(String employeesName, Integer idCode) {
        this.employeesName = employeesName;
        this.idCode = idCode;
    }

    public Long getEmployeesId() {
        return employeesId;
    }

    public void setEmployeesId(Long employeesId) {
        this.employeesId = employeesId;
    }

    public String getEmployeesName() {
        return employeesName;
    }

    public void setEmployeesName(String employeesName) {
        this.employeesName = employeesName;
    }

    public Integer getIdCode() {
        return idCode;
    }

    public void setIdCode(Integer idCode) {
        this.idCode = idCode;
    }

    public Set<Departments> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Departments> departments) {
        this.departments = departments;
    }
    
    public void addDepartment(Departments department) {
        departments.add(department);
        department.getEmployees().add(this);
    }
 
    public void removeDepartment(Departments department) {
        departments.remove(department);
        department.getEmployees().remove(this);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.employeesId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employees other = (Employees) obj;
        if (!Objects.equals(this.employeesId, other.employeesId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Employees{" + "employeesId=" + employeesId + ", employeesName=" + employeesName + ", idCode=" + idCode + '}';
    }

}
