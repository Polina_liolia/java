<%-- 
    Document   : index
    Created on : Nov 2, 2018, 10:58:40 PM
    Author     : Администратор
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <title>Send mail and SMS</title>
    </head>
    <body>
        <h1>Sending mail and SMS</h1>
        <form method="post" action="${pageContext.request.contextPath}/send">
            <div class="form-group">
                <label for="inputEmail">Email address</label>
                <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Enter email" required="true">
                <small id="emailHelp" class="form-text text-muted">Email is required</small>
            </div>
            <div class="form-group">
                <label for="inputPhone">Phone</label>
                <input type="phone" name="phone" class="form-control" id="inputPhone" placeholder="Enter phone number" required="true">
                <small id="phoneHelp" class="form-text text-muted">Phone number is required</small>
            </div>
            <div class="form-group">
                <label for="inputMsg">Message</label>
                <input type="text" name="message" class="form-control" id="inputMsg" placeholder="Enter message" required="true">
                <small id="phoneMsg" class="form-text text-muted">Message text is required</small>
            </div>
            <input type="submit" class="btn btn-primary" value="Send"/>
        </form>
    </body>
</html>
