package sending;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;


@WebServlet("/send")
public class SendingServlet extends HttpServlet {

    //sending mail 
    private static String USER_NAME = "azure.itstep24.kharkov";  // GMail user name (just the part before "@gmail.com")
    private static String PASSWORD = "Azure30$13"; // GMail password

    private static String RECIPIENT = "polina.liolia@gmail.com";

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String msg = request.getParameter("message");

        StringBuilder sb = new StringBuilder();

       // String mailResultMessage = this.sendFromGMail(USER_NAME, PASSWORD, new String[]{email, RECIPIENT}, "test", msg);
        //sb.append(mailResultMessage);

        String smsResultMessage = this.sendSms("e3507e00-7a83-4603-81cd-6684531c9d09", "BG8Mc3v5gESeRIjB1pBXeg==", phone, msg);
        sb.append(smsResultMessage);

        request.setAttribute("message",
                "There was an error: " + sb.toString());

        //sending sms
        getServletContext().getRequestDispatcher("/message.jsp").forward(
                request, response);
    }

    private String sendFromGMail(String from,
            String pass, String[] to, String subject, String body) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        // Сессия соединения с Вэб сервером
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        StringBuilder sb = new StringBuilder();
        sb.append("SMS sending result: ");
        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];
            // To get the array of addresses
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }
            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }
            message.setSubject(subject);
            message.setText(body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            sb.append("Success!");
        } catch (AddressException ae) {
            sb.append(ae.getMessage());
        } catch (MessagingException me) {
            sb.append(me.getMessage());
        }

        return sb.toString();
    }

    private String sendSms(String appKey, String appSecret, String phoneNumber, String message) {
       StringBuilder sb = new StringBuilder();
        sb.append("SMS sending result: ");
        try {
            URL url = new URL("https://messagingapi.sinch.com/v1/sms/" + phoneNumber);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            String userCredentials = "application\\" + appKey + ":" + appSecret;
            byte[] encoded = Base64.encodeBase64(userCredentials.getBytes());
            String basicAuth = "Basic " + new String(encoded);
            connection.setRequestProperty("Authorization", basicAuth);
            String postData = "{\"Message\":\"" + message + "\"}";
            OutputStream os = connection.getOutputStream();
            os.write(postData.getBytes());
            StringBuilder response = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ( (line = br.readLine()) != null)
                response.append(line);
            br.close();
            os.close();
            System.out.println(response.toString());
            sb.append("Success! ");
        } catch (Exception e) {
            sb.append(e.getMessage());
        }
        return sb.toString();
    }

}
