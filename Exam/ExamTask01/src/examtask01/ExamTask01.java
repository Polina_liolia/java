/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examtask01;

import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Администратор
 */
public class ExamTask01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        try {
            JSONObject json = JsonReader.readJsonFromUrl("http://headers.jsontest.com/");
            Iterator keys = json.sortedKeys();
            while(keys.hasNext()){
                System.out.println(keys.next());
            }
        } catch (JSONException ex) {
            Logger.getLogger(ExamTask01.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
