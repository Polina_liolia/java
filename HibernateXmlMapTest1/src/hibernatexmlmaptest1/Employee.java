package hibernatexmlmaptest1;

public class Employee {

    private long id = 1L;
    private String name;

    public Employee() {
    }
    
     public Employee(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
