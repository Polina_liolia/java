package code;

public class EmployeesHandler {
    private String employeesId;
    private String employees_Name;
    private String idCode;

    public EmployeesHandler() {
    }

    public String getEmployeesId() {
        return employeesId;
    }

    public void setEmployeesId(String employeesId) {
        this.employeesId = employeesId;
    }

    public String getEmployees_Name() {
        return employees_Name;
    }

    public void setEmployees_Name(String employees_Name) {
        this.employees_Name = employees_Name;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }
    
}
