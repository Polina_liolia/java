<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

<jsp:useBean id="beanEmployees" scope="session" class="code.EmployeesHandler"/>

<sql:setDataSource var="snapshot" 
                   driver="org.sqlite.JDBC"
                   url="jdbc:sqlite:D:\MyData\test_skj.db"
                   user="root"  password="pass123"/>
<sql:update dataSource="${snapshot}" var="result">
    DELETE FROM Employees WHERE EmployeesId = <jsp:getProperty name="beanEmployees" property="employeesId"></jsp:getProperty> 
</sql:update>
    
    <jsp:setProperty name="beanEmployees" property="employeesId"></jsp:setProperty>
   
    

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delete Employee</title>
    </head>
    <body>
        <h1>Employee with id =  <jsp:getProperty name="beanEmployees" property="employeesId"></jsp:getProperty> deleted! </h1>
    </body>
</html>
