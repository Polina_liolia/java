<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<sql:setDataSource var="snapshot" 
                   driver="org.sqlite.JDBC"
                   url="jdbc:sqlite:D:\MyData\test_skj.db"
                   user="root"  password="pass123"/>
<sql:query dataSource="${snapshot}" var="result">
   SELECT j.* from JOBS j
</sql:query>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <h1>Список подразделений:</h1>
        <table>
            <tr>
                <th>ID</th>
                <th>Job Title</th>
            </tr>
            <c:forEach var="row" items="${result.rows}">
                <tr>
                    <td><c:out value="${row.id}" /></td>
                    <td><c:out value="${row.JOB_TITLE}" /></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
