<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<sql:setDataSource var="snapshot" 
                   driver="org.sqlite.JDBC"
                   url="jdbc:sqlite:D:\MyData\test_skj.db"
                   user="root"  password="pass123"/>
<sql:query dataSource="${snapshot}" var="result">
   SELECT em.Id,men.LAST_NAME,men.BDATE,em.tab_number,men.PHONE,em.WORK_PHONE,j.JOB_TITLE,em.SALARY from Employee em left join MEN men on em.id_men = men.id left join JOBS j on em.id_jobs = j.id
</sql:query>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employee JSP Page</title>
    </head>
    <body>
        <h2>Укажите ID сотрудника для удаления</h2>
        <form method="get" action="EmployeeDelete.jsp">
            ID <input type="text" id="txtEmplId" name="employeesId">
            <input type="submit" value="Delete">
        </form>
        
        <h1>Список сотрудников:</h1>
        <table>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>BDate</th>
                <th>Tab Number</th>
                <th>Phone</th>
                <th>Work Phone</th>
                <th>Job</th>
                <th>Salary</th>
            </tr>
            <c:forEach var="row" items="${result.rows}">
                <tr>
                    <td><c:out value="${row.Id}" /></td>
                    <td><c:out value="${row.LAST_NAME}" /></td>
                    <td><c:out value="${row.BDATE}" /></td>
                    <td><c:out value="${row.tab_number}" /></td>
                    <td><c:out value="${row.PHONE}" /></td>
                    <td><c:out value="${row.WORK_PHONE}" /></td>
                    <td><c:out value="${row.JOB_TITLE}" /></td>
                    <td><c:out value="${row.SALARY}" /></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
