
package workwithfiles;

import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
 
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.*; //подключение всей библиотеки Swing - аналог WinForms
import java.awt.*;
import java.awt.event.*; // возможность обрабатывать клики



public class WorkWithFiles extends  JFrame //добавление возможности прослушивать события
{
    private JButton btnToClick;
  
    public WorkWithFiles() {
        super("Demo file type filter...");
        //
        // создаем панель. 
        JPanel p = new JPanel();
        this.add(p); 
        // к панели добавляем менеджер FlowLayout. 
        p.setLayout(new FlowLayout()); 
        // к панели добавляем кнопки. 
        p.add(new JButton("start 2")); 
        p.add(new JButton("start 2")); 
        p.add(new JButton("start 3")); 
        p.add(new JButton("start 4")); 
        p.add(new JButton("start 5")); 
        p.add(new JButton("start 6")); 
        p.add(new JButton("Okay"));
        //
        //InitializeComponent();
        
        
        //и изменение надписи на ней на "Clicked!"
        setSize(300, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        
        
    }
    
     private void InitializeComponent() {
         
         //создание кнопки
        btnToClick = new JButton("Click me!");
        btnToClick.addActionListener(
        new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                //и изменение надписи на ней на "Clicked!"
                btnToClick.setText("Clicked!");
                ShowOpenFileDialog();
            }
        }
        );
        
         //размещение компонентов в панели
        JPanel panel = new JPanel();
        //размещение Flowlayout
        panel.setLayout(new FlowLayout());
        panel.add(btnToClick);
        
        this.add(panel); 
     }
     
    private void ShowOpenFileDialog() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PDF Documents", "pdf"));
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("MS Office Documents", "docx", "xlsx", "pptx"));
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Images", "jpg", "png", "gif", "bmp"));
        fileChooser.setAcceptAllFileFilterUsed(true);
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            System.out.println("Selected file: " + selectedFile.getAbsolutePath());
        }
    }
     
    public static void main(String[] args) {
       
        new WorkWithFiles();
    
    }
    
}
