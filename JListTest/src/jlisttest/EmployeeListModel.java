package jlisttest;

import javax.swing.*;
import java.util.*;

public class EmployeeListModel extends AbstractListModel<MyUser>{

    protected List<MyUser> list;

    protected EmployeeListModel() {
        list = new ArrayList<MyUser>();
    }

    public EmployeeListModel(List<MyUser> list) {
        this.list = list;
    }
    
    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public MyUser getElementAt(int i) {
        return list.get(i);
    }
    
    //additional overriding:
    public void AddElement (MyUser item) {
        list.add(item);
        int index = list.size();
        //обновление данных внутри того, кто отображает данные
        fireContentsChanged(item, index, index); 
    }

    public void fireDataChanged()
    {
        int index = list.size();
    }
}
