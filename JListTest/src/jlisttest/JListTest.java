package jlisttest;

import javax.swing.JFrame;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.List;

public class JListTest extends JFrame {

    private JList<MyUser> listUser;
    private JList<String> listStr;
    private JLabel lbl1;
    private JButton btn1;

    public JListTest() {
        setSize(300, 400);
        //визуальный компонент JList
        this.listUser = new JList<MyUser>();
        this.listStr = new JList();
        this.lbl1 = new JLabel("Выделена строка: пусто");
        this.btn1 = new JButton("Очистить сообщение");

        this.setLayout(new BorderLayout());

        //панель кнопок
        JPanel panelBtns = new JPanel(new BorderLayout(WIDTH, WIDTH));
        panelBtns.add(this.lbl1, BorderLayout.CENTER);
        panelBtns.add(this.btn1, BorderLayout.SOUTH);
        this.add(panelBtns, BorderLayout.SOUTH);

        //список MyUser со скроллингом
        JScrollPane scrollPane = new JScrollPane(listUser);
        this.add(scrollPane, BorderLayout.CENTER);

        //список строк со скроллингом
        JScrollPane scrollPane2 = new JScrollPane(listStr);
        this.add(scrollPane2, BorderLayout.NORTH);

        //event hendlers
        //strings list selection changed
        listStr.addListSelectionListener(
                new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                if (!lse.getValueIsAdjusting()) {
                    List<String> selectedItems = listStr.getSelectedValuesList();
                    System.out.println(selectedItems);
                    lbl1.setText(selectedItems.get(0));
                }
            }
        }
        );

//        //event handler classic
//        listUser.addListSelectionListener(
//                new ListSelectionListener() {
//            @Override
//            public void valueChanged(ListSelectionEvent lse) {
//                if (!lse.getValueIsAdjusting()) {
//                    List<MyUser> selectedItems = listUser.getSelectedValuesList();
//                    System.out.println(selectedItems);
//                    lbl1.setText(selectedItems.get(0).toString());
//                }
//            }
//        }
//        );

        //event handler lambda
        listUser.addListSelectionListener((e)->{
            List<MyUser> selectedItems = listUser.getSelectedValuesList();
                    System.out.println(selectedItems);
                    lbl1.setText(selectedItems.get(0).toString());
        });

        //button clicked - classic way
        btn1.addActionListener(
                new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                lbl1.setText("Нет выделения в списке");
            }
        }
        );
        


//button clicked - lambda expression way
        btn1.addActionListener((e) -> {
            lbl1.setText("Нет выделения в списке");
        });

        //моя коллекция - то, что может прийти из БД
        //модель данных для визуального компонента, которая настроена на мои данные
        EmployeeListModel listModel = new EmployeeListModel();
        DefaultListModel<String> modelStrs = new DefaultListModel<>();

        listModel.AddElement(new MyUser("иванов", 10));
        listModel.AddElement(new MyUser("ивАнов", 50));
        listModel.AddElement(new MyUser("Иванов", 20));

        modelStrs.addElement("Петров");
        modelStrs.addElement("ивАнов");
        modelStrs.addElement("Иванов");
        modelStrs.addElement("Сидоров");

        listUser.setModel(listModel);
        listStr.setModel(modelStrs);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        JListTest form = new JListTest();
        form.setVisible(true);

    }
}
