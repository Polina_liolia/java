package jlisttest;

import java.util.Comparator;
import java.util.Objects;

public class MyUser implements Comparator<MyUser>{
    private String name;
    private int age;

    @Override
    public int compare(MyUser t, MyUser t1)
    {
        return (t.getAge() < t1.getAge()) ? -1 : 
                (t.getAge() > t1.getAge()) ? 1 : 0;
    }
    
    @Override
    public String toString() {
        return "MyUser{" + "name=" + name + ", age=" + age + '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public MyUser() {
    }

    public MyUser(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
