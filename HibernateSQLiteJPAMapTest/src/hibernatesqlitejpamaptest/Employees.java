package hibernatesqlitejpamaptest;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity; //@Entity
import javax.persistence.Id; //@Id
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "Employees")
public class Employees implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EmployeesId", unique = true, nullable = false)
    private long employeesId;
    
    @Column(name = "Employees_Name", unique = false, nullable = false)
    private String employees_Name;
    
    @Column(name = "IdCode", unique = true, nullable = false)
    private int idCode;
    
    @OneToMany(mappedBy = "employees")
    private List<DocsEntity> docs;

    public List<DocsEntity> getDocs() {
        return docs;
    }

    public void setDocs(List<DocsEntity> docs) {
        this.docs = docs;
    }

    public Employees() {
    }

    public Employees(String employees_Name, int idCode) {
        this.employees_Name = employees_Name;
        this.idCode = idCode;
    }

    public long getEmployeesId() {
        return employeesId;
    }

    public String getEmployees_Name() {
        return employees_Name;
    }

    public int getIdCode() {
        return idCode;
    }

    public void setEmployeesId(long employeesId) {
        this.employeesId = employeesId;
    }

    public void setEmployees_Name(String employees_Name) {
        this.employees_Name = employees_Name;
    }

    public void setIdCode(int idCode) {
        this.idCode = idCode;
    }
    
    @Override
    public String toString() {
        return "Employees{" + "employeesId=" + employeesId + ", employees_Name=" + employees_Name + ", idCode=" + idCode + '}';
    }
}
