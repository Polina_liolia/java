package hibernatesqlitejpamaptest;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class HibernateSQLiteJPAMapTest {

   public static void main(String[] args) {
        testEmployee();
        testEmployees();
        
    }
    
    private static void testEmployee() throws ExceptionInInitializerError, HibernateException {
        SessionFactory mFctory;
        try{
            mFctory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = mFctory.openSession();
        Transaction tx = null;
        Long employeeID = null;
        String fname ="test name 1";
        try{
            tx = session.beginTransaction();
            Employee employee = new Employee(fname);
            employeeID = (Long) session.save(employee);
            tx.commit();
        }catch (Exception e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        
    }
    
    private static void testEmployees() throws ExceptionInInitializerError, HibernateException {
        SessionFactory mFctory;
        try{
            mFctory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = mFctory.openSession();
        Transaction tx = null;
        Long employeeID = null;
        String fname ="test employees name 2";
        try{
            tx = session.beginTransaction();
            //здесь генерируется запрос
            //если new - то insert
            Employees employees = new Employees(fname, 12554);
            employeeID = (Long) session.save(employees);
            
            DocsEntity doc1 = new DocsEntity("новый паспорт", employees);
            session.save(doc1);
            tx.commit();
        }catch (Exception e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    
     
    
   

}
