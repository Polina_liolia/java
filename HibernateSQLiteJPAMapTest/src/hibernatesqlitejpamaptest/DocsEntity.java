package hibernatesqlitejpamaptest;

import java.io.Serializable;
import javax.persistence.Entity; //@Entity
import javax.persistence.Id; //@Id
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "DOCS")
public class DocsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    private long id;
    
    @Column(name = "Docs_Info", unique = true, nullable = false, length = 100)
    private String docs_info;
    
    @ManyToOne
    @JoinColumn(name = "EmployeesId")
    private Employees employees; //в БД EmployeesId
    
     public DocsEntity(String docs_info, Employees employees) {
        this.docs_info = docs_info;
        this.employees = employees;
    }

    public DocsEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDocs_info() {
        return docs_info;
    }

    public void setDocs_info(String docs_info) {
        this.docs_info = docs_info;
    }

    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }
   
}
