/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaconsoleapp;

import javax.swing.*; //подключение всей библиотеки Swing - аналог WinForms
import java.awt.*;
import java.awt.event.*; // возможность обрабатывать клики

/**
 *
 * @author asp
 */
public class MyWindow extends JFrame
        implements ActionListener //добавление возможности прослушивать события
{

    private JLabel lblMes;
    private JButton btnWork;
    private JButton btnYes;
    private JButton btnNo;

    public MyWindow() {
        super("First JFrame programm");
        InitializeComponent();
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    private void InitializeComponent() {
        //создание компонентов
        lblMes = new JLabel("Hello world!");
        btnWork = new JButton("Push me!");
        btnYes = new JButton("OK?");
        btnNo = new JButton("No!");

        //размещение компонентов в панели
        JPanel panel = new JPanel();
       
            /*BORDER LAYOUT*/
    
//        panel.setLayout(new BorderLayout());
//        panel.add(lblMes, BorderLayout.NORTH);
//        panel.add(btnWork, BorderLayout.NORTH);
//        panel.add(btnYes, BorderLayout.WEST);
//        panel.add(btnNo, BorderLayout.EAST);
        
           
        /*FLOW LAYOUT*/
       /* FlowLayout менеджер устанавливает компоненты слева направо и при заполнении переходит на строку вниз.*/
//        panel.setLayout(new FlowLayout());
//                
        /*GRID LAYOUT*/
        /*GridLayout это менеджер, который помещает компоненты в таблицу.*/
            
        // panel.setLayout(new GridLayout(2, 3));
        
        
        /*GRID BAG LAYOUT*/
        /*Этот менеджер подобно GridLayout менеджеру устанавливает компоненты в таблицу, но он более гибок,
        так как предоставляет возможность определять для компонентов разную ширину и высоту колонок и
        строк таблицы*/
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbConstraints = new GridBagConstraints();
       
        gbConstraints.ipady = 0; // установить первоночальный размер 
        gbConstraints.anchor = GridBagConstraints.CENTER;
        gbConstraints.weighty = 0.1; // установить отступ
        gbConstraints.weightx = 0.1; // установить отступ
        //gbConstraints.fill = GridBagConstraints.HORIZONTAL; //// натуральная высота, максимальная ширина
        
        gbConstraints.gridwidth = 2; // установить в 2 колонки
        gbConstraints.gridy = 1; // строка 1 
        gbConstraints.gridx = 1; //столбец 1
        panel.add(lblMes, gbConstraints);
        
        gbConstraints.gridwidth = 2; // установить в 2 колонки
        gbConstraints.gridy = 2; //строка 2
        gbConstraints.gridx = 1; //столбец 1
        panel.add(btnWork,gbConstraints);
        
        gbConstraints.gridwidth = 1; // установить в 1 колонку
        gbConstraints.gridy = 3; //строка 3
        gbConstraints.gridx = 1;  //столбец 1
        panel.add(btnYes, gbConstraints);
        
        gbConstraints.gridwidth = 1; // установить в 1 колонку
        gbConstraints.gridy = 3; //строка 3
        gbConstraints.gridx = 2; //столбец 2
        panel.add(btnNo, gbConstraints);

        this.add(panel);
      
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        btnWork.setText("Меня нажимали");
    }
}
