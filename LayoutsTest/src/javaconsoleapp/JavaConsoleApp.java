/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaconsoleapp;

import MyUser.MyUser;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asp
 */
public class JavaConsoleApp {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        System.out.println("Введите N:");
//        byte[] bytes = new byte[200];
//        try {
//            int count = System.in.read(bytes);
//        } catch (IOException ex) {
//            Logger.getLogger(JavaConsoleApp.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        double d = Double.parseDouble(new String(bytes));
//        System.out.println("double value is " + d);
//        int[] array = new int[5];
//        for(int i = 0; i < array.length; i++){
//            array[i] = i + 1;
//        }
//        for(double i : array) // Foreach, Foreach - внутри коллекции
//            System.out.println(i);
//        
//        double[] array2 = new double[5];        
//        for(int i = 0; i < array2.length; i++){
//            array2[i] = Math.random();
//        }
//        int myNum = 1;
//        System.out.println("before myNum = " + myNum);
//        System.out.println("orig.array2");
//        printArray(array2);    
//        System.out.println("after myNum = " + myNum);
//        changeValue(array2, myNum, -123);
//        System.out.println("new.array2");
//        printArray(array2);
//        
//        //library test
//        MyUser myUser = new MyUser(1, "someName");
//        System.out.println(myUser.getName());
        
        //window test
        MyWindow form = new MyWindow();
        form.setVisible(true);

    }
    private static void printArray(double[] array2) {
        for(double i : array2) // Foreach, Foreach - внутри коллекции
            System.out.println(i);
    }
    public static void changeValue(double[] array,
            int num,  //в C# есть ref, out, in
            //аналогов в JAVA нет, либо как строка, либо как массив
            double newValue){
        array[num] = newValue;
        num++;
    }
    public static void swp(int a, int b){
        int t = a;
        a = b;
        b = t;
    }
    public static void swp(int[] args){
        int t = args[0];
        args[0] = args[1];
        args[1] = t;
    }
}
//JDBC = ADO.NET
//Hibernate = Entity