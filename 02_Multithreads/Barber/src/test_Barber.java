
public class test_Barber {

	public static void main(String[] args) {
		int countOfCustomers = 3;
		int flowFrequency = 500;
		int timeForMakingHaircut = 2000;

		BarberShop barberShop = new BarberShop(timeForMakingHaircut);
		Thread barberThread = new Thread(new Barber(barberShop));
		barberThread.start();
		Thread arrOfThreads[] = new Thread[countOfCustomers];
		for (int i = 0; i < countOfCustomers; i++) {
			try {
				Thread.sleep(flowFrequency);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			arrOfThreads[i] = new Thread(new Consumer(barberShop));
			arrOfThreads[i].start();
		}
		for (Thread t : arrOfThreads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		barberThread.interrupt();
	}
}

class BarberShop {

	private static final int maxCountOfChairs = 5;
	private int currentCountOfChairs;
	private boolean chairOfBarber = true;
	private boolean hadAHaircutFlag;
	private int timeForMakingHaircut;

	public BarberShop(int time) {
		this.timeForMakingHaircut = time;
	}

	// for barber
	public synchronized void toMakeAHaircut(String name) throws InterruptedException {
		while (chairOfBarber) {
			wait();
		}
		// making a haircut
		try {
			Thread.sleep(timeForMakingHaircut);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(name + " made a haircut");
		chairOfBarber = true;
		notifyAll();
	}

	// for customers
	public synchronized void toGetAHaircut(String name) {
		while (!hadAHaircutFlag) {
			if (chairOfBarber) {
				System.out.println(name + " starts to get a haircut");
				chairOfBarber = false;
				notify();
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println(name + " had a haircut");
				hadAHaircutFlag = true;
			} else if (!isFull()) {
				System.out.println(name + " seats in reception");
				currentCountOfChairs++;
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				currentCountOfChairs--;
			}
		}
		System.out.println(name + " goes away");
		hadAHaircutFlag = false;
	}

	public boolean isFull() {
		return currentCountOfChairs == maxCountOfChairs;
	}

}

class Consumer implements Runnable {

	private final BarberShop barberShop;
	private String threadName = "Customer ";
	private static int count = 0;

	public Consumer(BarberShop barberShop) {
		count++;
		this.barberShop = barberShop;
		threadName += count;
	}

	@Override
	public void run() {
		barberShop.toGetAHaircut(threadName);
	}
}

class Barber implements Runnable {

	private final BarberShop barberShop;
	private String threadName = "Barber ";
	private static int count = 0;

	public Barber(BarberShop barberShop) {
		count++;
		this.barberShop = barberShop;
		threadName += count;
	}

	@Override
	public void run() {
		System.out.println("Barber shop is opened");
		Thread thread = Thread.currentThread();
		while (!thread.isInterrupted()) {
			try {
				barberShop.toMakeAHaircut(threadName);
			} catch (InterruptedException e) {
				System.out.println("Barber shop is closed");
				return;
			}
		}

	}
}
