package Lesson11.tpic1;


import static java.lang.Thread.sleep;

public class AnderControl implements Runnable{
    private static int i;

    public static int getI() {
        return i;
    }

    private static int iplus(){
        return i++;
    }

    @Override
    public void run() {
        while (true){
            iplus();
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
