package Lesson11.tpic1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Thread thread = new Thread(new AnderControl());

        int action;
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter number: \n" +
                "If \'1\' turn on the thread \n" +
                "If \'2\' stop the thread \n" +
                "If \'3\' out to console result of increment\n" +
                "If \'4\' exit the program \n");
        action = scanner.nextInt();
        while (action != 4){
            switch (action){
                case 1: if(thread.isAlive()){
                            thread.resume();
                            System.out.println("Thread recovering");
                    }
                        else{
                            thread.start();
                            System.out.println("Thread turn on!");
                    }
                    break;
                case 2: thread.suspend();
                        System.out.println("Thread stop");

                    break;
                case 3: System.out.println("Number is: "+ AnderControl.getI());
                    break;
                default:
                    break;
            }
            System.out.println("enter number: \n" +
                                "If \'1\' turn on the thread \n" +
                                "If \'2\' stop the thread \n" +
                                "If \'3\' out to console result of increment\n" +
                                "If \'4\' exit the program\n");
            action = scanner.nextInt();
            if(action == 4){
                thread.stop();
                System.out.println("Program is over");
            }
        }
    }
}
