/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ourtimer;

import java.util.logging.Level;
import java.util.logging.Logger;
import static ourtimer.OurTimer.jframeTimer;

/**
 *
 * @author DDIMAE
 */
public class OurTimer {

    static public JFrameTimer jframeTimer;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
	// TODO code application logic here
	/*System.out.println(" I am main Thread!!!");

	Counter counter = new Counter();
	Thread thr = new Thread(counter);
	thr.start();
	//thr.setPriority(Thread.MAX_PRIORITY);
	System.out.println("Поток главный " + Thread.currentThread().getName());
	System.out.println("Поток порожденный " + thr.getName());

	(new Thread("Мегапоток") {
	    public void run() {
		System.out.println("Я поток ");// + getName());
		try {
		    Thread.sleep(5000);
		} catch (InterruptedException ex) {
		    Logger.getLogger(OurTimer.class.getName()).log(Level.SEVERE, null, ex);
		}
                System.out.println("*** Я поток закончил работу! ***");//+getName()+" ");
	    }

	}).start();

	for (int i = 1; i < 21; i++) {
	    System.out.println("иииии ...." + i);
	    try {
		Thread.sleep(1000);
	    } catch (InterruptedException ex) {
		Logger.getLogger(OurTimer.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}*/

	//*********************************************
	/*
	int n = 5000;
	int[] arr = new int[n];
	for (int i = 0; i < n; i++) {
	    arr[i] = i;
	}
	CalcSum[] listThreads = new CalcSum[5];
	listThreads[0] = new CalcSum(arr, 0, 1000, "OT_0_D0_999");
	listThreads[1] = new CalcSum(arr, 1000, 2000, "OT_1000_D0_1999");
	listThreads[2] = new CalcSum(arr, 2000, 3000, "OT_2000_D0_2999");
	listThreads[3] = new CalcSum(arr, 3000, 4000, "OT_3000_D0_3999");
	listThreads[4] = new CalcSum(arr, 4000, 5000, "OT_4000_D0_4999");

	for (int i = 0; i < 5; i++) {
	    listThreads[i].run();
	    //listThreads[i].join();
	}

	for (int i = 0; i < 5; i++) {
	    listThreads[i].join();
	}

	//*********************************
	int sum = 0;
	for (int i = 0; i < 5; i++) {
	    sum += listThreads[i].sumR;
	}
	System.out.println("==== Сумма всех элементов равна ===" + sum);
	System.out.println("\nThat`s ALL!!!");
	 */
	//*********************************************
	//Запускаем таймер
	/* Set the Nimbus look and feel */
	//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
	/* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
	 */
	try {
	    for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
		if ("Nimbus".equals(info.getName())) {
		    javax.swing.UIManager.setLookAndFeel(info.getClassName());
		    break;
		}
	    }
	} catch (ClassNotFoundException ex) {
	    java.util.logging.Logger.getLogger(JFrameTimer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	} catch (InstantiationException ex) {
	    java.util.logging.Logger.getLogger(JFrameTimer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	} catch (IllegalAccessException ex) {
	    java.util.logging.Logger.getLogger(JFrameTimer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	} catch (javax.swing.UnsupportedLookAndFeelException ex) {
	    java.util.logging.Logger.getLogger(JFrameTimer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	}
	//</editor-fold>

	/* Create and display the form */
	java.awt.EventQueue.invokeLater(new Runnable() {
	    public void run() {
		jframeTimer = new JFrameTimer();
		JFrameTimer.setButtons(false, true, false, false);
		jframeTimer.setVisible(true);
	    }
	});

    }

}

class Counter implements Runnable {

    @Override
    public void run() {
	System.out.println("Я мегасчетчик!!!");
	for (int i = 1; i < 21; i++) {
	    System.out.println("==== " + i);
	    try {
		Thread.sleep(1000);
	    } catch (InterruptedException ex) {
		Logger.getLogger(OurTimer.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	System.out.println("\nЯ мегасчетчик - работу закончил!!!");
    }

}

class CalcSum extends Thread {

    int[] mas;
    int start;
    int finish;
    int sumR;

    public CalcSum(int[] mas, int start, int finish, String string) {
	super(string);
	this.mas = mas;
	this.start = start;
	this.finish = finish;
    }

    int sumRange() {
	System.out.println("Суммируем элементы от " + start + " до " + finish);
	int sum = 0;
	for (int i = start; i < finish; i++) {
	    sum += mas[i];
	}
	System.out.println("Сумма от " + start + " до " + finish + " равна " + sum);
	return sum;
    }

    @Override
    public void run() {
	sumR = sumRange();
    }

}

//class SecCounter implements Runnable { // не могу получить доступ к методам!!!
class SecCounter extends Thread {
    private volatile boolean pausa; //= false;
    private volatile boolean isStop; //= false;
    private int ksec;

    public SecCounter() {
	ksec = 0;
	pausa = false;
	isStop = false;
    }
    
    public boolean isPausa(){
	return pausa;
    }
    
    public void setPaused() //Меняет действие на противоположное
    {
	pausa = !pausa;
    }

    public void finish() //Инициирует завершение потока
    {
	isStop = true;
    }

    

    public void incSecCounter() {
	ksec++;
    }

    @Override
    public void run() {
        System.out.println("==== Начал отсчет ====");
	do {
	    
	    while (!pausa) {
		incSecCounter();
		System.out.println("==== " + ksec);
		try {
		    Thread.sleep(1000);
		    jframeTimer.getjTextField1().setText("" + ksec);
		} catch (InterruptedException ex) {
		    //Logger.getLogger(OurTimer.class.getName()).log(Level.SEVERE, null, ex);
		}
	    }
	    System.out.println("----- пауза ------");
	} while (!isStop);
	System.out.println("==== Отчет окончен ====");
    }
}
