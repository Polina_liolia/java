package Lesson11.topic2;

import java.util.concurrent.Semaphore;

import static java.lang.Thread.sleep;

public class Potok2 implements Runnable{
    Semaphore sem;
    String name;

    public Potok2(String name, Semaphore sem){
        this.name = name;
        this.sem = sem;
    }
    @Override
    public void run() {

            try {
                while (Bank.n < 100 && Bank.n != 0) {
                    System.out.println(name + " ask to access");
                    sem.acquire();
                    Bank.bringN();
                    System.out.println(name+" bring money. There are stay " + Bank.n + "$");
                    sem.release();
                    sleep(8000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }
}
