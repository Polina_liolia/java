package Lesson11.topic2;

import java.util.concurrent.Semaphore;

import static java.lang.Thread.sleep;

public class Potol1 implements Runnable{
    String name;
    Semaphore sem;


    public Potol1(String name, Semaphore sem){
        this.name = name;
        this.sem = sem;
    }

    @Override
    public void run() {

            try {
                while (Bank.n<100 && Bank.n != 0) {
                    System.out.println(name + " ask to access");

                    sem.acquire();
                    Bank.getN();
                    System.out.println(name + " get money. There are stay " + Bank.n + " $");
                    sem.release();
                    sleep(2000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(Bank.n == 0)
                System.out.println("Money is over");
    }
}
