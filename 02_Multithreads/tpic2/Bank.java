package Lesson11.topic2;

import java.util.concurrent.Semaphore;

public class Bank {
    static volatile int n = 10;
    public static void main(String[] args) {

        Semaphore semaphore = new Semaphore(1);
        Potol1 potol1 = new Potol1("Client", semaphore);
        Potok2 potok2 = new Potok2("Encashment", semaphore);

        Thread thread1 = new Thread(potol1);
        Thread thread2 = new Thread(potok2);

        thread1.start();
        thread2.start();
    }
    static void getN(){
        n--;
    }
    static void bringN(){
        n = n + 3;
    }
}
