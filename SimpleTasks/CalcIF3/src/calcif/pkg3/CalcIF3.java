package calcif.pkg3;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class CalcIF3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        3 Дано целое число A. Проверить истинность высказывания: «Число A является четным». 
        */
         System.out.println("Input integer...");
        Scanner in = new Scanner(System.in);
        int A = in.nextInt();
        if(A%2 == 0)
        {
            System.out.println("A is even - true");
        }
        else
        {
            System.out.println("A is even - false");
        }
    }
    
}
