package cycle1;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class Cycle1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       /*1 Даны целые числа K и N (N > 0). Вывести N раз число K. */
        System.out.println("Input integer...");
        Scanner in = new Scanner(System.in);
        int K = in.nextInt();
        System.out.println("How many times would you like to print this integer?");
        int N = in.nextInt();
        for(int i = 0; i < N; i++)
            System.out.print(K);
    }
    
}
