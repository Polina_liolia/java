
package calcif.pkg1;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class CalcIF1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        1 Дано целое число A. Проверить истинность высказывания: «Число A является
положительным». 
        */
        System.out.println("Input integer...");
        Scanner in = new Scanner(System.in);
        int A = in.nextInt();
        if(A > 0)
        {
            System.out.println("A is positive - true");
        }
        else
        {
            System.out.println("A is positive - false");
        }
    }
    
}
