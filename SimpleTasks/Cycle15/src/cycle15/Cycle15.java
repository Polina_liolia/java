package cycle15;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class Cycle15 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        15 Дано целое число N (> 0). Найти сумму
                11 + 22 + … + NN
        Чтобы избежать целочисленного переполнения, вычислять слагаемые этой суммы с
        помощью вещественной переменной и выводить результат как вещественное число. 
        */
        System.out.print("Input integer...");
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        double result = 0;
        for (int i = 1; i <= N; i++)
            result += Math.pow(i, i);
        System.out.println("Result " + result);
    }
    
}
