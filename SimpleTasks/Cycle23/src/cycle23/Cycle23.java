package cycle23;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class Cycle23 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       /*
        23 Дано целое число N (> 1). Если оно является простым, т. е. не имеет положительных
делителей, кроме 1 и самого себя, то вывести true, иначе вывести false.
        */
       int N = -1;
        do {
            System.out.print("Input integer (>1)...");
            Scanner in = new Scanner(System.in);
            N = in.nextInt();
        } while (N <= 1);
        
        boolean flag = true;
        for(int i = N; i > 0; i--)
        {
            if(N%i == 0 && i != N && i != 1)
            {
                flag = false;
                break;
            }
        }
        System.out.print(flag);
    }
    
}
