package function_9;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class Function_9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        9. Описать функцию IsPower5(K) логического типа, возвращающую True, если целый
параметр K (> 0) является степенью числа 5, и False в противном случае. С ее помощью найти
количество степеней числа 5 в наборе из 10 целых положительных чисел. 
         */
        int K;
        do {
            System.out.print("Input positive integer...");
            Scanner in = new Scanner(System.in);
            K = in.nextInt();
        } while (K <= 0);
        boolean result = IsPower5(K);
        System.out.println(K + " is " + (!result ? "not " : "") + "a power of 5.");

        int[] arr = {25, 1, 44, 125, 678, 3, 5, 76, 9, 625};
        int count = 0;
        for (int i : arr) {
            if (IsPower5(i)) {
                count++;
            }
        }
        System.out.println("Array {25, 1, 44, 125, 678, 3, 5, 76, 9, 625} contains " + count + " powers of 5");
    }

    public static boolean IsPower5(int K) {
        if (K <= 1) {
            return false;
        }
        boolean flag = true;
        while (K > 1) {
            if (K % 5 != 0) {
                flag = false;
                break;
            }
            K /= 5;
        }
        return flag;
    }
}
