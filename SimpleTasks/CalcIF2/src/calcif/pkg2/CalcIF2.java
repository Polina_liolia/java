/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcif.pkg2;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class CalcIF2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        2 Дано целое число A. Проверить истинность высказывания: «Число A является нечетным». 
        */
        System.out.println("Input integer...");
        Scanner in = new Scanner(System.in);
        int A = in.nextInt();
        if(A%2 != 0)
        {
            System.out.println("A is odd - true");
        }
        else
        {
            System.out.println("A is odd - false");
        }
    }
    
}
