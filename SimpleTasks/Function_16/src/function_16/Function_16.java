package function_16;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class Function_16 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        16. Описать функцию Fact(N) вещественного типа, вычисляющую значение факториала
N! = 1·2·…·N (N > 0 — параметр целого типа; вещественное возвращаемое значение
используется для того, чтобы избежать целочисленного переполнения при больших
значениях N). С помощью этой функции найти факториалы пяти данных целых чисел.
        */
        for(int i = 0; i< 5; i++)
        {
            int N;
            do {
                System.out.print("Input positive integer...");
                Scanner in = new Scanner(System.in);
                N = in.nextInt();
            } while (N <= 0 );
            double factorial = Fact(N);
            System.out.println("Factorial of " + N + " is " + factorial);
        }
        
            
    }
    public static double Fact(int N)
    {
        double result = 0;
        for(int i = 1; i <= N; i++ )
        {
            result += i;
        }
        return result;
    }
    
}
