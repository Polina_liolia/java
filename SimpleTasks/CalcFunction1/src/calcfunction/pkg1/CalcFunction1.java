/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcfunction.pkg1;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class CalcFunction1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //1 Поменять местами содержимое переменных A и B и вывести новые значения A и B. 
        System.out.println("Input two integers...");
        Scanner in = new Scanner(System.in);
        int A, B;
        A = in.nextInt();
        B = in.nextInt();

        int tmp = A;
        A = B;
        B = tmp;
        
        System.out.println("A: " + A);
        System.out.println("B: " + B);
        
        
       
    }
    
}
