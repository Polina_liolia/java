package calcif5;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class CalcIF5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        5 Даны два целых числа: A, B. Проверить истинность высказывания: «Справедливы
неравенства A ≥ 0 или B < −2». 
        */
    System.out.println("Input two integers...");
        Scanner in = new Scanner(System.in);
        int A = in.nextInt();
        int B = in.nextInt();
        if(A >= 0 || B < -2)
        {
            System.out.println("true");
        }
        else
        {
             System.out.println("false");
        }
    
    }
    
}
