package function_14;

import java.security.InvalidParameterException;
import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class Function_14 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        14. Описать функцию DegToRad(D) вещественного типа, находящую величину угла в радианах,
если дана его величина D в градусах (D — вещественное число, 0 ≤ D < 360). Воспользоваться
следующим соотношением: 180° = π радианов. В качестве значения π использовать 3.14. С
помощью функции DegToRad перевести из градусов в радианы пять данных углов. 
         */
        for (int i = 0; i < 5; i++) {
            double D;
            do {
                System.out.print("Input degrees (0; 360)...");
                Scanner in = new Scanner(System.in);
                D = in.nextDouble();
            } while (D < 0 || D > 360);
            double rad = DegToRad(D);
            System.out.println(D + " degrees = " + rad + " radians");
        }
    }

    public static double DegToRad(double D) {
        if (D < 0) {
            throw new InvalidParameterException();
        }
        if (D > 360) {
            D = D % 360;
        }
        return D * 3.14 / 180;
    }
}
