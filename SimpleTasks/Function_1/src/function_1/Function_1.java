package function_1;

/**
 *
 * @author Администратор
 */
public class Function_1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        1. Описать функцию Mean(X, Y, AMean, GMean), вычисляющую среднее арифметическое
AMean = (X+Y)/2 и среднее геометрическое GMean = (X·Y)
1/2 двух положительных чисел X и Y
(X и Y — входные, AMean и GMean — выходные параметры вещественного типа). С помощью
этой процедуры найти среднее арифметическое и среднее геометрическое для пар (A, B),
(A, C), (A, D), если даны A, B, C, D.
         */
        double A = 10.0,
                B = 5.0,
                C = 3.0,
                D = 6.0;
        ObjectHolder AMean = new ObjectHolder(-1.0);
        ObjectHolder GMean = new ObjectHolder(-1.0); 
        
        Mean(A, B, AMean, GMean);
        System.out.println("A and B: ");
        System.out.println("AMean = " + AMean.param + " GMean = " + GMean.param);
        
        Mean(A, C, AMean, GMean);
        System.out.println("A and C: ");
        System.out.println("AMean = " + AMean.param + " GMean = " + GMean.param);
        
        Mean(A, D, AMean, GMean);
        System.out.println("A and D: ");
        System.out.println("AMean = " + AMean.param + " GMean = " + GMean.param);
        
    }

    /**
     *
     * @param X
     * @param Y
     * @param AMean
     * @param GMean
     */
    public static void Mean(double X, double Y, ObjectHolder AMean, ObjectHolder GMean) {
        AMean.param = (X + Y) / 2.0;
        GMean.param = Math.sqrt(X * Y);
    }

     /**
  * ObjectHolder (Generic ParameterWrapper)
  */
 public static class ObjectHolder<T> {
    public ObjectHolder(T param) {
     this.param=param;
    }
    public T param;
 }
}
