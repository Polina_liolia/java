package cycle19;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class Cycle19 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        19 Дано целое число N (> 0). Используя операции деления нацело и взятия остатка от
            деления, найти количество и сумму его цифр.
         */
        int N = -1;
        do {
            System.out.print("Input positive integer...");
            Scanner in = new Scanner(System.in);
            N = in.nextInt();
        } while (N <= 0);
        int sum = 0;
        int count = 0;
        int number = N;
        do
        {
            sum += number%10;
            count++;
            number /= 10;
        }while(number > 0);
        System.out.println(N + " consists of " + count + " digits, sum of this digits is " + sum);
    }

}
