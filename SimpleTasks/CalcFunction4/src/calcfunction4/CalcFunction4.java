package calcfunction4;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class CalcFunction4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       /*
        4 Дано значение температуры T в градусах Фаренгейта. Определить значение этой же
температуры в градусах Цельсия. Температура по Цельсию TC и температура по
Фаренгейту TF связаны следующим соотношением:
TC = (TF − 32)·5/9.
        */
       
        System.out.println("Input temperature in F...");
        Scanner in = new Scanner(System.in);
        double TF = in.nextDouble();
        double TC = (TF - 32) * 5 / 9;
        System.out.println("TF: " + TF + "; TC: " + TC);
    }
    
}
