package function_3;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class Function_3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        3. Описать функцию InvDigits(K), меняющую порядок следования цифр целого положительного
числа K на обратный (K — параметр целого типа, являющийся одновременно входным и
выходным). С помощью этой процедуры поменять порядок следования цифр на обратный для
каждого из пяти данных целых чисел. 
         */
        for(int i = 0; i< 5; i++)
        {
            int K;
            do {
                System.out.print("Input positive integer...");
                Scanner in = new Scanner(System.in);
                K = in.nextInt();
            } while (K <= 0 );
            IntHolder intHolder = new IntHolder(K);
            InvDigits(intHolder);
            System.out.println(K + " inverted: " + intHolder.param);
        }
    }

    public static void InvDigits(IntHolder K)
    {
        int number = K.param;
        int inverted = 0;
        while(number > 0)
        {
            inverted = inverted * 10 + number%10;
            number /= 10;
        }
        K.param = inverted;
    }

    public static class IntHolder {

        public IntHolder (int param) {
            this.param = param;
        }
        public int param;
    }
}
