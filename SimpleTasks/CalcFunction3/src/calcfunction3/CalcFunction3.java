/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcfunction3;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class CalcFunction3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        3 Даны переменные A, B, C. Изменить их значения, переместив содержимое A в C, C — в B, B
            — в A, и вывести новые значения переменных A, B, C.*/
        
        System.out.println("Input three integers...");
        Scanner in = new Scanner(System.in);
        int A, B, C;
        A = in.nextInt();
        B = in.nextInt();
        C = in.nextInt();

        int tmp = C;
        C = A;
        A = B;
        B = tmp;
        
        System.out.println("A: " + A);
        System.out.println("B: " + B);
        System.out.println("C: " + C);
    }
    
}
