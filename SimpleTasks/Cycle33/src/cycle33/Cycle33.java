package cycle33;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class Cycle33 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        33 Спортсмен-лыжник начал тренировки, пробежав в первый день 10 км. Каждый
следующий день он увеличивал длину пробега на P процентов от пробега
предыдущего дня (P — вещественное, 0 < P < 50). По данному P определить, после
какого дня суммарный пробег лыжника за все дни превысит 200 км, и вывести
найденное количество дней K (целое) и суммарный пробег S (вещественное число). 
        */
        
        double P;
        do {
            System.out.print("Input percent (0; 50)...");
            Scanner in = new Scanner(System.in);
            P = in.nextDouble();
        } while (P <= 0  || P >= 50);
        double S = 10.0;
        int K = 1;
        while (S <= 200)
        {
            S *= 1 + P / 100;
            K++;
        }
        System.out.print("Количество дней: " +  K + ", суммарный пробег: " + S);
    }
    
}
