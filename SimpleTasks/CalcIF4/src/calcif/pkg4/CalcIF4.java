package calcif.pkg4;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class CalcIF4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        4 Даны два целых числа: A, B. Проверить истинность высказывания: «Справедливы
неравенства A > 2 и B ≤ 3». 
        */
        System.out.println("Input two integers...");
        Scanner in = new Scanner(System.in);
        int A = in.nextInt();
        int B = in.nextInt();
        if(A > 2 && B <= 3)
        {
            System.out.println("true");
        }
        else
        {
             System.out.println("false");
        }
    }
    
}
