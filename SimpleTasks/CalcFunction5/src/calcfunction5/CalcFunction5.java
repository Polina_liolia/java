package calcfunction5;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class CalcFunction5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       /*
        5 Дано значение температуры T в градусах Цельсия. Определить значение этой же
температуры в градусах Фаренгейта. Температура по Цельсию TC и температура по
Фаренгейту TF связаны следующим соотношением:
TC = (TF − 32)·5/9.
        */
        System.out.println("Input temperature in C...");
        Scanner in = new Scanner(System.in);
        double TC = in.nextDouble();
        double TF = TC  * 9 / 5 + 32;
        System.out.println("TF: " + TF + "; TC: " + TC);
    }
    
}
