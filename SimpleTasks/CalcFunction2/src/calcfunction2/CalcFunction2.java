package calcfunction2;

import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class CalcFunction2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*2 Даны переменные A, B, C. Изменить их значения, переместив содержимое A в B, B — в C, C
            — в A, и вывести новые значения переменных A, B, C.  */
        System.out.println("Input three integers...");
        Scanner in = new Scanner(System.in);
        int A, B, C;
        A = in.nextInt();
        B = in.nextInt();
        C = in.nextInt();

        int tmp = B;
        B = A;
        A = C;
        C = tmp;
        
        System.out.println("A: " + A);
        System.out.println("B: " + B);
        System.out.println("C: " + C);
        
    }
    
}
