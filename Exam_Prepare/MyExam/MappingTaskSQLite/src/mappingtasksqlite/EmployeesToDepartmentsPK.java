package mappingtasksqlite;

import java.io.Serializable;
import javax.persistence.Embeddable;

@Embeddable
public class EmployeesToDepartmentsPK implements Serializable {

    public EmployeesToDepartmentsPK() {
    }

    @Override
    public int hashCode() {
        int hash = 0;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeesToDepartmentsPK)) {
            return false;
        }
        EmployeesToDepartmentsPK other = (EmployeesToDepartmentsPK) object;
        return true;
    }
}
