package mappingtasksqlite;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "EmployeesToDepartments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmployeesToDepartments.findAll", query = "SELECT e FROM EmployeesToDepartments e"),
    @NamedQuery(name = "EmployeesToDepartments.findById", query = "SELECT e FROM EmployeesToDepartments e WHERE e.id = :id"),
    @NamedQuery(name = "EmployeesToDepartments.findByEmployeesId", query = "SELECT e FROM EmployeesToDepartments e WHERE e.employeesId = :employeesId"),
    @NamedQuery(name = "EmployeesToDepartments.findByDepartmentsId", query = "SELECT e FROM EmployeesToDepartments e WHERE e.departmentsId = :departmentsId")})
public class EmployeesToDepartments implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EmployeesToDepartmentsPK employeesToDepartmentsPK;
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "EmployeesId")
    private int employeesId;
    @Basic(optional = false)
    @Column(name = "DepartmentsId")
    private int departmentsId;

    public EmployeesToDepartments() {
    }

    public EmployeesToDepartments(EmployeesToDepartmentsPK employeesToDepartmentsPK) {
        this.employeesToDepartmentsPK = employeesToDepartmentsPK;
    }

    public EmployeesToDepartments(EmployeesToDepartmentsPK employeesToDepartmentsPK, int employeesId, int departmentsId) {
        this.employeesToDepartmentsPK = employeesToDepartmentsPK;
        this.employeesId = employeesId;
        this.departmentsId = departmentsId;
    }

    public EmployeesToDepartmentsPK getEmployeesToDepartmentsPK() {
        return employeesToDepartmentsPK;
    }

    public void setEmployeesToDepartmentsPK(EmployeesToDepartmentsPK employeesToDepartmentsPK) {
        this.employeesToDepartmentsPK = employeesToDepartmentsPK;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getEmployeesId() {
        return employeesId;
    }

    public void setEmployeesId(int employeesId) {
        this.employeesId = employeesId;
    }

    public int getDepartmentsId() {
        return departmentsId;
    }

    public void setDepartmentsId(int departmentsId) {
        this.departmentsId = departmentsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (employeesToDepartmentsPK != null ? employeesToDepartmentsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeesToDepartments)) {
            return false;
        }
        EmployeesToDepartments other = (EmployeesToDepartments) object;
        if ((this.employeesToDepartmentsPK == null && other.employeesToDepartmentsPK != null) || (this.employeesToDepartmentsPK != null && !this.employeesToDepartmentsPK.equals(other.employeesToDepartmentsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mappingtasksqlite.EmployeesToDepartments[ employeesToDepartmentsPK=" + employeesToDepartmentsPK + " ]";
    }

}
