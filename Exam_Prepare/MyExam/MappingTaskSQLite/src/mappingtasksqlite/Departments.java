package mappingtasksqlite;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "Departments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Departments.findAll", query = "SELECT d FROM Departments d"),
    @NamedQuery(name = "Departments.findByDepartmentsID", query = "SELECT d FROM Departments d WHERE d.departmentsID = :departmentsID"),
    @NamedQuery(name = "Departments.findByDepartmentsName", query = "SELECT d FROM Departments d WHERE d.departmentsName = :departmentsName")})
public class Departments implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DepartmentsPK departmentsPK;
    @Column(name = "DepartmentsID")
    private Integer departmentsID;
    @Basic(optional = false)
    @Column(name = "Departments_Name")
    private String departmentsName;

    public Departments() {
    }

    public Departments(DepartmentsPK departmentsPK) {
        this.departmentsPK = departmentsPK;
    }

    public Departments(DepartmentsPK departmentsPK, String departmentsName) {
        this.departmentsPK = departmentsPK;
        this.departmentsName = departmentsName;
    }

    public DepartmentsPK getDepartmentsPK() {
        return departmentsPK;
    }

    public void setDepartmentsPK(DepartmentsPK departmentsPK) {
        this.departmentsPK = departmentsPK;
    }

    public Integer getDepartmentsID() {
        return departmentsID;
    }

    public void setDepartmentsID(Integer departmentsID) {
        this.departmentsID = departmentsID;
    }

    public String getDepartmentsName() {
        return departmentsName;
    }

    public void setDepartmentsName(String departmentsName) {
        this.departmentsName = departmentsName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (departmentsPK != null ? departmentsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Departments)) {
            return false;
        }
        Departments other = (Departments) object;
        if ((this.departmentsPK == null && other.departmentsPK != null) || (this.departmentsPK != null && !this.departmentsPK.equals(other.departmentsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mappingtasksqlite.Departments[ departmentsPK=" + departmentsPK + " ]";
    }

}
