package mappingtasksqlite;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "Employees")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Employees.findAll", query = "SELECT e FROM Employees e"),
    @NamedQuery(name = "Employees.findByEmployeesId", query = "SELECT e FROM Employees e WHERE e.employeesId = :employeesId"),
    @NamedQuery(name = "Employees.findByEmployeesName", query = "SELECT e FROM Employees e WHERE e.employeesName = :employeesName"),
    @NamedQuery(name = "Employees.findByIdCode", query = "SELECT e FROM Employees e WHERE e.idCode = :idCode")})
public class Employees implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EmployeesPK employeesPK;
    @Column(name = "EmployeesId")
    private Integer employeesId;
    @Basic(optional = false)
    @Column(name = "Employees_Name")
    private String employeesName;
    @Column(name = "IdCode")
    private Integer idCode;

    public Employees() {
    }

    public Employees(EmployeesPK employeesPK) {
        this.employeesPK = employeesPK;
    }

    public Employees(EmployeesPK employeesPK, String employeesName) {
        this.employeesPK = employeesPK;
        this.employeesName = employeesName;
    }

    public EmployeesPK getEmployeesPK() {
        return employeesPK;
    }

    public void setEmployeesPK(EmployeesPK employeesPK) {
        this.employeesPK = employeesPK;
    }

    public Integer getEmployeesId() {
        return employeesId;
    }

    public void setEmployeesId(Integer employeesId) {
        this.employeesId = employeesId;
    }

    public String getEmployeesName() {
        return employeesName;
    }

    public void setEmployeesName(String employeesName) {
        this.employeesName = employeesName;
    }

    public Integer getIdCode() {
        return idCode;
    }

    public void setIdCode(Integer idCode) {
        this.idCode = idCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (employeesPK != null ? employeesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employees)) {
            return false;
        }
        Employees other = (Employees) object;
        if ((this.employeesPK == null && other.employeesPK != null) || (this.employeesPK != null && !this.employeesPK.equals(other.employeesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mappingtasksqlite.Employees[ employeesPK=" + employeesPK + " ]";
    }

}
