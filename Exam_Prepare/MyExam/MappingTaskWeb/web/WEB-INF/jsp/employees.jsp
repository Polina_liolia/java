<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%--<%@ page session="false" %>--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="employee" scope="session" class="model.pojo.Employees"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employees</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>

        <h1>Employees list:</h1>
        <div class="col-xs-12">
        <div class="text-right">
             <a class="btn btn-primary btn-lg" href="<c:url value='/employees/add' />" >Add employee</a>
        </div>
    </div>
        
        <table class="table">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Employee Name</th>
                <th scope="col">ID Code</th>
                <th width="col">Edit</th>
                <th width="col">Delete</th>
            </tr>
            <c:forEach var="employee" items="${employees}" >
                <tr>
                    <td>
                        ${employee.getEmployeesId()}
                    </td>
                    <td>
                        ${employee.getEmployeesName()}
                    </td>
                    <td>
                        ${employee.getIdCode()}
                    </td>
                    <td>
                        <a class="btn btn-outline-success" href="<c:url value='/employees/edit?id=${employee.employeesId}' />" >Edit</a>
                    </td>
                    <td>
                        <a class="btn btn-outline-danger" href="<c:url value='/employees/remove?id=${employee.employeesId}' />" >Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
       <div class="col-xs-12">
        <div class="text-right">
             <a class="btn btn-info btn-lg" href="<c:url value='/' />" >Home</a>
        </div>
    </body>
</html>

