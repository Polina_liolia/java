<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Departments</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>
        <h1>Departments list:</h1>
        <table class="table">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Department Name</th>
            </tr>
            <c:forEach var="dept" items="${departments}">
            <%--  departments – это имя ключа в коллекции внутри EmployeeController--%>
                <tr>
                    <td>
                        <c:out value="${dept.getDepartmentsID()}"></c:out>
                    </td>
                    <td>
                         <c:out value="${dept.getDepartmentsName()}"></c:out>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
