/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.ArrayList;
import java.util.List;
import model.pojo.Employees;
import org.hibernate.Query;
import org.hibernate.Session;

public class EmployeesDAO extends AbstractModelDAO {

    public static List<Employees> getEmployeesList() {
        List<Employees> lst;
        EmployeesDAO worker = new EmployeesDAO();
        Session session = null;
        try {
            session = worker.openSession();
            String hql = "from Employees";
            Query query = session.createQuery(hql);
            lst = query.list();
        } catch (Exception e) {
            lst = new ArrayList<>();
        } finally {
            worker.closeSession(session);
        }
        return lst;
    }

    public static void addEmployee(Employees empl) {
        EmployeesDAO worker = new EmployeesDAO();
        Session session = null;
        try {
            session = worker.openSession();
            session.persist(empl);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            worker.closeSession(session);
        }
    }

    public static void updateEmployee(Employees empl) {
        EmployeesDAO worker = new EmployeesDAO();
        Session session = null;
        try {
            session = worker.openSession();
            session.update(empl);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            worker.closeSession(session);
        }
    }

    public static Employees getEmployeeById(long id) {
        Employees empl = null;
        EmployeesDAO worker = new EmployeesDAO();
        Session session = null;
        try {
            session = worker.openSession();
            empl = (Employees) session.get(Employees.class, id);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            worker.closeSession(session);
        }
        return empl;
    }

    public static void removeEmployee(long id) {
        EmployeesDAO worker = new EmployeesDAO();
        Session session = null;
        try {
            session = worker.openSession();
            Employees empl = (Employees) session.load(Employees.class, id);
            if (null != empl) {
                session.delete(empl);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            worker.closeSession(session);
        }
    }
}
