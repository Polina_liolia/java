package model.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public abstract class AbstractModelDAO {

    protected ServiceRegistry serviceRegistry;

//open session
    public Session openSession() throws HibernateException {
        // loads configuration and mappings
        Configuration configuration = new Configuration().configure();
        ServiceRegistryBuilder registry = new ServiceRegistryBuilder();
        registry.applySettings(configuration.getProperties());
        serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        // builds a session factory from the service registry
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        // obtains the session
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        return session;
    }
    
//close session
    public void closeSession(Session session) throws HibernateException {
        session.getTransaction().commit();
        session.close();
        //отдал сборщику мусор - пометил на удаление
        StandardServiceRegistryBuilder.destroy(serviceRegistry);
    }
}
