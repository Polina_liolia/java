package model.pojo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


public class Departments implements Serializable {

    private Long departmentsID;
    private String departmentsName;
    private Set<Employees> employees = new HashSet<>();

    public Departments() {
    }

    public Departments(String departmentsName) {
        this.departmentsName = departmentsName;
    }

    public Long getDepartmentsID() {
        return departmentsID;
    }

    public void setDepartmentsID(Long departmentsID) {
        this.departmentsID = departmentsID;
    }

    public String getDepartmentsName() {
        return departmentsName;
    }

    public void setDepartmentsName(String departmentsName) {
        this.departmentsName = departmentsName;
    }

    public Set<Employees> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employees> employees) {
        this.employees = employees;
    }
    
    public void addEmployee(Employees employee) {
        employees.add(employee);
        employee.getDepartments().add(this);
    }
 
    public void removeEmployee(Employees employee) {
        employees.remove(employee);
        employee.getDepartments().remove(this);
    }
    
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.departmentsID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Departments other = (Departments) obj;
        if (!Objects.equals(this.departmentsID, other.departmentsID)) {
            return false;
        }
        return true;
    }

   

    @Override
    public String toString() {
        return "ID=" + departmentsID + ", name=" + departmentsName;
    }
}
