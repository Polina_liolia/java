package mappingtask;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class EmployeesHelper {
    public EntityManager em = Persistence.createEntityManagerFactory("MappingTaskPU").createEntityManager();

    public Employees add(Employees employee){
        em.getTransaction().begin();
        Employees employeeFromDB = em.merge(employee);
        em.getTransaction().commit();
        TypedQuery<Employees> namedQuery = em.createNamedQuery("Employees.findFull", Employees.class);
        namedQuery.setParameter("idCode", employeeFromDB.getIdCode());
        namedQuery.setParameter("employeesName", employeeFromDB.getEmployeesName());
        employeeFromDB = namedQuery.getSingleResult();
        return employeeFromDB;
    }

    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    public Employees get(long id){
        return em.find(Employees.class, id);
    }

    public void update(Employees employee){
        em.getTransaction().begin();
        em.merge(employee);
        em.getTransaction().commit();
    }

    public List<Employees> getAll(){
        TypedQuery<Employees> namedQuery = em.createNamedQuery("Employees.findAll", Employees.class);
        return namedQuery.getResultList();
    }
    
     public void close(){
         em.close();
     }
}
