package mappingtask;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class DepartmentsHelper {
 public EntityManager em = Persistence.createEntityManagerFactory("MappingTaskPU").createEntityManager();

    public Departments add(Departments department){
        em.getTransaction().begin();
        Departments departmentFromDB = em.merge(department);
        em.getTransaction().commit();
        return departmentFromDB;
    }

    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    public Departments get(long id){
        return em.find(Departments.class, id);
    }

    public void update(Departments department){
        em.getTransaction().begin();
        em.merge(department);
        em.getTransaction().commit();
    }

    public List<Departments> getAll(){
        TypedQuery<Departments> namedQuery = em.createNamedQuery("Departments.findAll", Departments.class);
        return namedQuery.getResultList();
    }
    
     public List<Departments> getByName(String deptName){
        TypedQuery<Departments> namedQuery = em.createNamedQuery("Departments.findByDepartmentsName", Departments.class)
                .setParameter("departmentsName", deptName);
        return namedQuery.getResultList();
    }
     
     public void close(){
         em.close();
     }
}
