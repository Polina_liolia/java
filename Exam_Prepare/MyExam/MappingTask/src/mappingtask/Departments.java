package mappingtask;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "Departments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Departments.findAll", query = "SELECT d FROM Departments d"),
    @NamedQuery(name = "Departments.findByDepartmentsID", query = "SELECT d FROM Departments d WHERE d.departmentsID = :departmentsID"),
    @NamedQuery(name = "Departments.findByDepartmentsName", query = "SELECT d FROM Departments d WHERE d.departmentsName = :departmentsName")})
public class Departments implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "DepartmentsID")
    private Long departmentsID;
    @Basic(optional = false)
    @Column(name = "Departments_Name")
    private String departmentsName;

    public Departments() {
    }

    public Departments(String departmentsName) {
        this.departmentsName = departmentsName;
    }

    
//    @ManyToMany(cascade = { 
//        CascadeType.PERSIST, 
//        CascadeType.MERGE
//    })
//    @JoinTable(name = "EmployeesToDepartments",
//        joinColumns = @JoinColumn(name = "DepartmentsID"),
//        inverseJoinColumns = @JoinColumn(name = "EmployeesId")
//    )
    @ManyToMany(mappedBy = "departments")
    private Set<Employees> employees = new HashSet<>() ;

    public void addEmployee(Employees employee) {
        employees.add(employee);
        employee.getDepartments().add(this);
    }
 
    public void removeEmployee(Employees employee) {
        employees.remove(employee);
        employee.getDepartments().remove(this);
    }

    public Set<Employees> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employees> employees) {
        this.employees = employees;
    }
   

    public Long getDepartmentsID() {
        return departmentsID;
    }

    public void setDepartmentsID(Long departmentsID) {
        Long oldDepartmentsID = this.departmentsID;
        this.departmentsID = departmentsID;
        changeSupport.firePropertyChange("departmentsID", oldDepartmentsID, departmentsID);
    }

    public String getDepartmentsName() {
        return departmentsName;
    }

    public void setDepartmentsName(String departmentsName) {
        String oldDepartmentsName = this.departmentsName;
        this.departmentsName = departmentsName;
        changeSupport.firePropertyChange("departmentsName", oldDepartmentsName, departmentsName);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.departmentsID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Departments other = (Departments) obj;
        if (!Objects.equals(this.departmentsID, other.departmentsID)) {
            return false;
        }
        return true;
    }

   

    @Override
    public String toString() {
        return "ID=" + departmentsID + ", name=" + departmentsName;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

   

}
