package mappingtask;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "Employees")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Employees.findAll", query = "SELECT e FROM Employees e"),
    @NamedQuery(name = "Employees.findByEmployeesId", query = "SELECT e FROM Employees e WHERE e.employeesId = :employeesId"),
    @NamedQuery(name = "Employees.findByEmployeesName", query = "SELECT e FROM Employees e WHERE e.employeesName = :employeesName"),
    @NamedQuery(name = "Employees.findByIdCode", query = "SELECT e FROM Employees e WHERE e.idCode = :idCode"),
    @NamedQuery(name = "Employees.findFull", query = "SELECT e FROM Employees e WHERE e.idCode = :idCode AND e.employeesName = :employeesName")})
public class Employees implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EmployeesId")
    private Long employeesId;
    @Basic(optional = false)
    @Column(name = "Employees_Name")
    private String employeesName;
    @Column(name = "IdCode")
    private Integer idCode;

    public Employees() {
    }

  
    public Employees(String employeesName) {
        this.employeesName = employeesName;
    }

    public Employees(String employeesName, Integer idCode) {
        this.employeesName = employeesName;
        this.idCode = idCode;
    }
    
    
    @ManyToMany(cascade = { 
        CascadeType.PERSIST, 
        CascadeType.MERGE
    })
    @JoinTable(name = "EmployeesToDepartments",
        joinColumns = @JoinColumn(name = "EmployeesId"),
        inverseJoinColumns = @JoinColumn(name = "DepartmentsID")
    )
    private Set<Departments> departments = new HashSet<>();
    
    
    public void addDepartment(Departments department) {
        departments.add(department);
        department.getEmployees().add(this);
    }
 
    public void removeDepartment(Departments department) {
        departments.remove(department);
        department.getEmployees().remove(this);
    }

    public Set<Departments> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Departments> departments) {
        this.departments = departments;
    }
   

    public Long getEmployeesId() {
        return employeesId;
    }

    public void setEmployeesId(Long employeesId) {
        Long oldEmployeesId = this.employeesId;
        this.employeesId = employeesId;
        changeSupport.firePropertyChange("employeesId", oldEmployeesId, employeesId);
    }

    public String getEmployeesName() {
        return employeesName;
    }

    public void setEmployeesName(String employeesName) {
        String oldEmployeesName = this.employeesName;
        this.employeesName = employeesName;
        changeSupport.firePropertyChange("employeesName", oldEmployeesName, employeesName);
    }

    public Integer getIdCode() {
        return idCode;
    }

    public void setIdCode(Integer idCode) {
        Integer oldIdCode = this.idCode;
        this.idCode = idCode;
        changeSupport.firePropertyChange("idCode", oldIdCode, idCode);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.employeesId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employees other = (Employees) obj;
        if (!Objects.equals(this.employeesId, other.employeesId)) {
            return false;
        }
        return true;
    }

   

    @Override
    public String toString() {
        return "Employees{" + "employeesId=" + employeesId + ", employeesName=" + employeesName + ", idCode=" + idCode + '}';
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    

}
