package UI;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import mappingtask.Employees;

public class EmployeeTableModel extends AbstractTableModel {
    
    List<Employees> employees;
    
    //columns description
    String[] columns = new String[]{
        "Id", "Name", "ID Code"
    };
    
    //columns types
    Class[] columnsType = new Class[]{
        Long.class, String.class, Integer.class
    };

    //constructor takes list of employees
    public EmployeeTableModel(List<Employees> employees) {
        this.employees = employees;
    }

    public Employees getEmployees(int rowIndex)
    {
        return this.employees.get(rowIndex);
    }
    
    @Override
    public int getRowCount() {
        return this.employees.size();
    }

    @Override
    public int getColumnCount() {
        return this.columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
         Employees currentEmployees = employees.get(rowIndex);
         return 0 == columnIndex ? currentEmployees.getEmployeesId():
                1 == columnIndex ? currentEmployees.getEmployeesName():
                2 == columnIndex ? currentEmployees.getIdCode() : null;
    }
    
    @Override
    public String getColumnName(int column) {
        return columns[column];
    }
    
    @Override
    public Class<?> getColumnClass(int column)
    {
        return columnsType[column];
    }
}