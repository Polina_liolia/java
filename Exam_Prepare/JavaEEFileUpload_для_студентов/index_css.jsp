<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page: File Upload Demo</title>
        <style>
            .fileform { 
    background-color: #FFFFFF;
    border: 1px solid #CCCCCC;
    border-radius: 2px;
    cursor: pointer;
    height: 26px;
    overflow: hidden;
    padding: 2px;
    position: relative;
    text-align: left;
    vertical-align: middle;
    width: 230px;
}
 
.fileform .selectbutton { 
    background-color: #A2A3A3;
    border: 1px solid #939494;
    border-radius: 2px;
    color: #FFFFFF;
    float: right;
    font-size: 16px;
    height: 20px;
    line-height: 20px;
    overflow: hidden;
    padding: 2px 6px;
    text-align: center;
    vertical-align: middle;
    width: 50px;
}
 
.fileform #upload{
    position:absolute; 
    top:0; 
    left:0; 
    width:100%; 
    -moz-opacity: 0; 
    filter: alpha(opacity=0); 
    opacity: 0; 
    font-size: 150px; 
    height: 30px; 
    z-index:20;
}
            .fileform #fileformlabel { 
background-color: #FFFFFF;
float: left;
height: 22px;
line-height: 22px;
overflow: hidden;
padding: 2px;
text-align: left;
vertical-align: middle;
width:160px;
}
        </style>
        <script type="text/javascript">
function getName (str){
    if (str.lastIndexOf('\\')){
        var i = str.lastIndexOf('\\')+1;
    }
    else{
        var i = str.lastIndexOf('/')+1;
    }						
    var filename = str.slice(i);			
    var uploaded = document.getElementById("fileformlabel");
    uploaded.innerHTML = filename;
}
        </script>
    </head>
    <body>
        
      

        
        
   <center>
        <form method="post" action="uploadFile" enctype="multipart/form-data">
            Select file to upload:
<div class="fileform">
<div id="fileformlabel"></div>
<div class="selectbutton">Обзор</div>
<input type="file" name="upload" id="upload" onchange="getName(this.value);" />
</div>
            <br/><br/>
            <input type="submit" value="btnUpload" />
        </form>
    </center>
    </body>
</html>
