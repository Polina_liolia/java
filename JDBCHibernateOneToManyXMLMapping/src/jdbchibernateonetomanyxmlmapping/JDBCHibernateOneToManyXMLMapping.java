package jdbchibernateonetomanyxmlmapping;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class JDBCHibernateOneToManyXMLMapping {

    public static void main(String[] args) {
       testProductXMLMapping();
        testCategoryXMLMapping();

        //testCategoryJDBCMapping();
    }

    private static void testProductXMLMapping() throws ExceptionInInitializerError, HibernateException {
        SessionFactory mFctory;
        try {
            mFctory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = mFctory.openSession();
        Transaction tx = null;
        Long productID = null;
        try {
            tx = session.beginTransaction();
            Product product = new Product("prod", "some description...", 10.2f, 1);
            productID = (Long) session.save(product);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    private static void testCategoryXMLMapping() throws ExceptionInInitializerError, HibernateException {
        SessionFactory mFctory;
        try {
            mFctory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = mFctory.openSession();
        Transaction tx = null;
        Long categoryID = null;

        try {
            tx = session.beginTransaction();
            Category category = new Category("new category");
            categoryID = (Long) session.save(category);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    private static void testCategoryJDBCMapping() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("test_skj");
        EntityManager entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();

        Category category = new Category("new category JDBC");
        entityManager.persist(category);

        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally{
            entityManager.close();
        factory.close();
        }
    }

}
