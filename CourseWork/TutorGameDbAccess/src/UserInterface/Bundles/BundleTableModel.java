package UserInterface.Bundles;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import tutorgamedbaccess.Bundle;

public class BundleTableModel extends AbstractTableModel {

    List<Bundle> bundles;
    
    //columns definition
    String[] columns = new String[]{
       "Id", "Asset name", "Bundle path", "Complexity level",  "Name", "Word"
    };
    private Class[] columnsTypes = new Class[]{
        Long.class, String.class, String.class, Integer.class, String.class, String.class
    };

    public BundleTableModel(List<Bundle> bundles) {
        this.bundles = bundles;
    }
    
    @Override
    public int getRowCount() {
        return bundles.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Bundle currentBundle = bundles.get(rowIndex);
        return 0 == columnIndex ? currentBundle.getId(): 
               1 == columnIndex ? currentBundle.getAssetName() :
               2 == columnIndex ? currentBundle.getBundle_path() : 
               3 == columnIndex ? currentBundle.getComplexityLevel() :
               4 == columnIndex ? currentBundle.getName() : 
               5 == columnIndex ? currentBundle.getWord() : null;
    }
    @Override
    public String getColumnName(int column) {
        return columns[column];
    }
    
    @Override
    public Class<?> getColumnClass(int column)
    {
        return columnsTypes[column];
    }

    public Bundle getBundle(int row) {
        return bundles.get(row);
    }

}
