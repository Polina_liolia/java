package UserInterface.Users;

import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import tutorgamedbaccess.User;

public class UserTableModel extends AbstractTableModel {
    
    List<User> users;
    
    //columns description
    String[] columns = new String[]{
        "Name", "Birth date", "Complexity level", "Help available", "Score"
    };
    
    //columns types
    Class[] columnsType = new Class[]{
        String.class, Date.class, Integer.class, Integer.class, Long.class
    };

    //constructor takes list of users
    public UserTableModel(List<User> users) {
        this.users = users;
    }

    public User getUser(int rowIndex)
    {
        return this.users.get(rowIndex);
    }
    
    @Override
    public int getRowCount() {
        return this.users.size();
    }

    @Override
    public int getColumnCount() {
        return this.columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
         User currentUser = users.get(rowIndex);
         return 0 == columnIndex ? currentUser.getName() :
                1 == columnIndex ? currentUser.getBirthDate():
                2 == columnIndex ? currentUser.getComplexityLevel() :
                3 == columnIndex ? currentUser.getHelpAvailable() :
                4 == columnIndex ? currentUser.getScore() : null;
    }
    
    @Override
    public String getColumnName(int column) {
        return columns[column];
    }
    
    @Override
    public Class<?> getColumnClass(int column)
    {
        return columnsType[column];
    }
}