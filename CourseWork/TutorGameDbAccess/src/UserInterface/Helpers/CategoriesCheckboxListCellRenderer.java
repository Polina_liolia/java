package UserInterface.Helpers;

import java.awt.Component;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import tutorgamedbaccess.Category;

public class CategoriesCheckboxListCellRenderer extends JCheckBox implements ListCellRenderer {

    public Component getListCellRendererComponent(JList list, Object value, int index, 
            boolean isSelected, boolean cellHasFocus) {

        setComponentOrientation(list.getComponentOrientation());
        setFont(list.getFont());
        setBackground(list.getBackground());
        setForeground(list.getForeground());
        setSelected(isSelected);
        setEnabled(list.isEnabled());
        
        if(value instanceof Category){
            Category category = (Category)value;
            setText(category.getName());
        }
        else{
            setText(value == null ? "" : value.toString());
        }
        return this;
    }
}