package UserInterface.Helpers;

import java.util.List;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import tutorgamedbaccess.Category;

public class CategoriesListModel implements ListModel{

    List<Category> categories;

    public CategoriesListModel(List<Category> categories) {
        this.categories = categories;
    }
    
    @Override
    public int getSize() {
        return categories.size();
    }

    @Override
    public Object getElementAt(int index) {
        return categories.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        
    }

}
