package UserInterface.Categories;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import tutorgamedbaccess.Category;

public class CategoryTableModel extends AbstractTableModel {

    List<Category> categories;

    //columns definition
    String[] columns = new String[]{
        "name", "description"
    };

    //columns types
    Class[] columnsTypes = new Class[]{
        String.class, String.class
    };

    //constructor takes a list of categories
    public CategoryTableModel(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public int getRowCount() {
        return categories.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Category currentCategory = categories.get(rowIndex);
        return 0 == columnIndex ? currentCategory.getName() :
               1 == columnIndex ? currentCategory.getDescription() : null;
    }
    @Override
    public String getColumnName(int column) {
        return columns[column];
    }
    
    @Override
    public Class<?> getColumnClass(int column)
    {
        return columnsTypes[column];
    }

    public Category getCategory(int row) {
        return categories.get(row);
    }

}
