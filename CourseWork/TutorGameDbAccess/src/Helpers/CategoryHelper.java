package Helpers;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import tutorgamedbaccess.Category;

public class CategoryHelper {
public EntityManager em = Persistence.createEntityManagerFactory("TutorGameDbAccessPU").createEntityManager();

    public Category add(Category category){
        em.getTransaction().begin();
        Category categoryFromDB = em.merge(category);
        em.getTransaction().commit();
        return categoryFromDB;
    }

    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    public Category get(long id){
        return em.find(Category.class, id);
    }

    public void update(Category category){
        em.getTransaction().begin();
        em.merge(category);
        em.getTransaction().commit();
    }

    public List<Category> getAll(){
        TypedQuery<Category> namedQuery = em.createNamedQuery("Category.getAll", Category.class);
        return namedQuery.getResultList();
    }
    
    public List<Category> getCateroriesForUser(){
        List<Category> categories = this.getAll();
         for(int i = 0; i < categories.size(); i++)
        {
            String categoryName = categories.get(i).getName();
            if(categoryName.equals("MysteryBox") ||  categoryName.equals("SuccessSound") || categoryName.equals("LooseSound"))
            {
                categories.remove(categories.get(i));
                i--;
            }
        }
         return categories;
    }
}
