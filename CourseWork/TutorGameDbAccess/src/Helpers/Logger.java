package Helpers;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

public class Logger implements Runnable{

    private final ReentrantLock locker;
    private final String path;
    private final String action;
    private final String whoCalled;
    private final String type;


    public Logger(ReentrantLock locker, String path, String action, String whoCalled, String type) {
        this.locker = locker;
        this.path = path;
        this.action = action;
        this.whoCalled = whoCalled;
        this.type = type;
    }
   
    
    public void WriteProtocol() {
        locker.lock();
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new FileWriter(path, true));
            writer.append(new Date().toString());
            writer.append("  ");
            writer.append(action);
            writer.append("  ");
            writer.append(whoCalled);
            writer.append("  ");
            writer.append(type);
            writer.println("");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            writer.close();
            locker.unlock();
            
        }
    }

    @Override
    public void run() {
       this.WriteProtocol();
    }
}
