package tutorgamedbaccess;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id; 
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

@Entity
@NamedQuery(name = "User.getAll", query = "SELECT u from User u")
@Table(name="Users")
public class User implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date birthDate;
    private int complexityLevel = 1;
    private int helpAvailable = 5;
    private long score = 0;

    public User() {
    }

    public User(String name, Date birthDate, int complexityLevel, int helpAvailable, long score) {
        this.name = name;
        this.birthDate = birthDate;
        this.complexityLevel = complexityLevel;
        this.helpAvailable = helpAvailable;
        this.score = score;
    }

    public User(String name, Date birthDate) {
        this.name = name;
        this.birthDate = birthDate;
    }
    
    @ManyToMany(cascade = { 
        CascadeType.PERSIST, 
        CascadeType.MERGE
    })
    @JoinTable(name = "CategoryUsers",
        joinColumns = @JoinColumn(name = "UserId"),
        inverseJoinColumns = @JoinColumn(name = "CategoryId")
    )
    private Set<Category> categories = new HashSet<>();
    
    public void addCategory(Category category) {
        categories.add(category);
    }
 
    public void removeCategory(Category category) {
        categories.remove(category);
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        changeSupport.firePropertyChange("name", oldName, name);
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        Date oldBirthDate = this.birthDate;
        this.birthDate = birthDate;
        changeSupport.firePropertyChange("birthDate", oldBirthDate, birthDate);
    }

    public int getComplexityLevel() {
        return complexityLevel;
    }

    public void setComplexityLevel(int complexityLevel) {
        int oldComplexityLevel = this.complexityLevel;
        this.complexityLevel = complexityLevel;
        changeSupport.firePropertyChange("complexityLevel", oldComplexityLevel, complexityLevel);
    }

    public int getHelpAvailable() {
        return helpAvailable;
    }

    public void setHelpAvailable(int helpAvailable) {
        int oldHelpAvailable = this.helpAvailable;
        this.helpAvailable = helpAvailable;
        changeSupport.firePropertyChange("helpAvailable", oldHelpAvailable, helpAvailable);
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        long oldScore = this.score;
        this.score = score;
        changeSupport.firePropertyChange("score", oldScore, score);
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", birthDate=" + birthDate + ", complexityLevel=" + complexityLevel + ", helpAvailable=" + helpAvailable + ", score=" + score + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 89 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
    
    
}
