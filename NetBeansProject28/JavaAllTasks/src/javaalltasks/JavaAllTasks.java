/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaalltasks;

/**
 *
 * @author asp
 */
public class JavaAllTasks {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("тест на положительное число:");
        System.out.println(CalcIF1isPositiveNumber(5));
        System.out.println( CalcIF1isPositiveNumber(-1));
        System.out.println(CalcIF1isPositiveNumber(0));
        
        System.out.println("тест на нечетное число:");
        System.out.println("число 5 " + CalcIF2isOddNumber(5));
        System.out.println("число 2 " + CalcIF2isOddNumber(2));
        System.out.println("тест на четное число:");
        System.out.println("число 5 " + CalcIF3EvenNumber(5));
        System.out.println("число 2 " + CalcIF3EvenNumber(2));
    }
    
    //Задачи по теме условия
    //Во всех заданиях данной группы требуется вывести логическое значение True, 
    ///если приведенное высказывание для предложенных исходных данных является истинным, 
    //и значение False в противном случае. Все числа, для которых указано количество цифр 
    //(двузначное число, трехзначное число и т. д.), считаются целыми положительными.
    
    //Число A является положительным
    public static boolean CalcIF1isPositiveNumber(int i)
    {
        return i>=0;
    }
    
    //Число A является нечетным
    public static boolean CalcIF2isOddNumber(int i)
    {
        return i%2 > 0;
    }
    
    //Число A является четным
    public static boolean CalcIF3EvenNumber(int i)
    {
        return i%2 == 0;
    }
}
