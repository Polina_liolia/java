/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaconsoleapp; //для первого занятия - это аналог немспейса

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author asp
 */
public class JavaConsoleApp {

    /**
     * @param args the command line arguments
     */
    
    //java - всегда консоль, всегла функция main, для визуального интерфейса - необходимо наследоваться
    public static void main(String[] args) {
        // TODO code application logic here
        double [] array = new double [5];
        int a = 3;
        int b = 4;
        for(int i =0; i< array.length; i++) 
        {
            //array[i] = i+1;
            array[i] = Math.random();
        }
        System.out.println("origin array:");
        printArray(array);
        int myNum = 1;
        System.out.println("my num " + myNum);
        changeValue(array,1, -123);
        System.out.println("new array:");
        printArray(array);
        System.out.println("my num " + myNum);
        swp(a,b);
        System.out.println("a= " + a + "b= "+ b);
        
        byte[]bytes = new byte[200]; //окружить оператор try, catch
        
        try {
            int count = System.in.read(bytes);
        } catch (IOException ex) {
            Logger.getLogger(JavaConsoleApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        double d = Double.parseDouble(new String (bytes));
        System.out.print("double value is "+ d);
        int N = (int)Math.round(d);
    }

    //static - так как функция main статическая
    private static void printArray(double[] array) {
        for (double i : array) // foreach, ForEach= внутри коллекций
            System.out.println(i);
    }
    
    public static void changeValue(double[] array, int num, double newValue)
    {
        array[num] = newValue;
        num++; //в с# есть ref, out, in, но в java такого нет: либо строка и преобразования, либо массив
        //классы Integer, Double, Number - классы обертки - для баз данных 
        //если нужно написать функцию, которая меняет местами значения двух переменных - нужно писать функцию, которая принимает массив.
    }
    public static void swp(int a, int b)
    {
        int t = a;
        a = b;
        b = t;
    }
    public static void swp(int[]args)
    {
        int t = args [0];
        args[0] = args[1];
        args[1] = t;
    }
    
}
