package UserInterface;

import hibernatejpamapping.Jobs;
import hibernatejpamapping.Men;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class JobsWindow extends JFrame {

    JTable tblJobs;
    List<Jobs> jobsList;
    
    public JobsWindow() {
        super("Jobs list");
        InitializeComponent();
    }

    private void InitializeComponent() {
        setSize(400, 400);
        //getting data from DB: 
        SessionFactory mFctory;
        try {
            mFctory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = mFctory.openSession();
        Transaction tx = null;
        jobsList = new ArrayList<Jobs>();
        try {
            tx = session.beginTransaction();
            SQLQuery query = session
                    .createSQLQuery("Select id, JOB_TITlE from `JOBS`");
            List<Object[]> rows = query.list();
            for (Object[] row : rows) {
                Jobs job = new Jobs();
                if(row[0] != null)
                    job.setId(Integer.parseInt(row[0].toString()));
                if(row[1] != null)
                    job.setJob_title(row[1].toString());
                jobsList.add(job);
            }
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        
        //data model creating:
        JobsTableModel model = new JobsTableModel(jobsList);
        
        //creating table:
        tblJobs = new JTable(model);
        
        //setting layout:
        setLayout(new BorderLayout());
        
        //adding table to layout:
        add(new JScrollPane(tblJobs), BorderLayout.CENTER);
        
         setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
