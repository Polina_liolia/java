package UserInterface;

import hibernatejpamapping.Men;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class MenTableModel extends AbstractTableModel{

    List<Men> men;
    
    //columns definition:
    String[] columns = new String[]{
        "First name", "Last name", "Middle name", "Phone", "Birth date"
    };
    
    //columns types:
    Class[] columnsTypes = new Class[]{
        String.class, String.class, String.class, String.class, String.class
    };

    public MenTableModel(List<Men> men) {
        this.men = men;
    }
   
    
    @Override
    public int getRowCount() {
        return men.size();
    }

    @Override
    public int getColumnCount() {
       return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Men currentRow = men.get(rowIndex);
        return columnIndex == 0 ? currentRow.getFirst_name():
                columnIndex == 1 ? currentRow.getLast_name() :
                columnIndex == 2 ? currentRow.getMiddle_name() : 
                columnIndex == 3 ? currentRow.getPhone() : 
                columnIndex == 4 ? currentRow.getBdate() : null;
    }
    
    @Override
    public String getColumnName(int column) {
        return columns[column];
    }
    
    @Override
    public Class<?> getColumnClass(int column)
    {
        return columnsTypes[column];
    }


}
