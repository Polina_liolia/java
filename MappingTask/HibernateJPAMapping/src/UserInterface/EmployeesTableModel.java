package UserInterface;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import hibernatejpamapping.Employee;

public class EmployeesTableModel extends AbstractTableModel {

    List<Employee> employees;
    
    //columns definition:
    String[] columns = new String[]{
        "Tab number", "First name", "Last name","Work phone", "Job", "Salary" 
    };
    
    //columns types:
    Class[] columnsTypes = new Class[]{
        String.class, String.class, String.class, String.class, String.class, Double.class
    };

    public EmployeesTableModel(List<Employee> employees) {
        this.employees = employees;
    }
    
    @Override
    public int getRowCount() {
        return employees.size();
    }

    @Override
    public int getColumnCount() {
         return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Employee currentRow = employees.get(rowIndex);
        return columnIndex == 0 ? currentRow.getTab_number() : 
                columnIndex == 1 && currentRow.getMen() != null ? currentRow.getMen().getFirst_name() : 
                columnIndex == 2 && currentRow.getMen() != null ? currentRow.getMen().getLast_name():
                columnIndex == 3 ? currentRow.getWork_phone() :
                columnIndex == 4 && currentRow.getJob() != null ? currentRow.getJob().getJob_title(): 
                columnIndex == 5 ? currentRow.getSalary() : 
                 null;
    }
    
     @Override
    public String getColumnName(int column) {
        return columns[column];
    }
    
    @Override
    public Class<?> getColumnClass(int column)
    {
        return columnsTypes[column];
    }
}
