package UserInterface;

import hibernatejpamapping.User;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class UserTableModel extends AbstractTableModel {

    List<User> users;
    
    //columns definition:
    String[] columns = new String[]{
        "Id", "First name", "Last name"
    };
    
    //columns types:
    Class[] columnsTypes = new Class[]{
        Integer.class, String.class, String.class
    };

    public UserTableModel(List<User> users) {
        this.users = users;
    }
    
    
    
    @Override
    public int getRowCount() {
        return users.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
     User currentRow = users.get(rowIndex);
     return columnIndex == 0 ? currentRow.getUser_id() : 
             columnIndex == 1 ? currentRow.getFirst_name() :
             columnIndex == 2 ? currentRow.getLast_name() : null;
    }
    
    @Override
    public String getColumnName(int column) {
        return columns[column];
    }
    
    @Override
    public Class<?> getColumnClass(int column)
    {
        return columnsTypes[column];
    }

}
