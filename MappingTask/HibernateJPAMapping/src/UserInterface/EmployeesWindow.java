package UserInterface;

import hibernatejpamapping.Employee;
import hibernatejpamapping.Jobs;
import hibernatejpamapping.Men;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class EmployeesWindow extends JFrame {
    
    JTable tblEmployees;
    List<Employee> employeesList;

    public EmployeesWindow() {
        super("Employees list");
        InitializeComponent();
    }
    
    private void InitializeComponent() {
        setSize(400, 400);
        //getting data from DB: 
        SessionFactory mFctory;
        try {
            mFctory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = mFctory.openSession();
        Transaction tx = null;
        employeesList = new ArrayList<Employee>();
        try {
            tx = session.beginTransaction();
            SQLQuery query = session
                    .createSQLQuery("Select tab_number, WORK_PHONE, id_jobs, SALARY, id_men from `Employee`");
            List<Object[]> rows = query.list();
            for (Object[] row : rows) {
                Employee empl = new Employee();
                if(row[0] != null)
                    empl.setTab_number(row[0].toString());
                if(row[1] != null)
                    empl.setWork_phone(row[1].toString());
                if(row[2] != null)
                {
                    long job_id = Integer.parseInt(row[2].toString());
                    Jobs job =  (Jobs) session.get(Jobs.class, job_id);
                    empl.setJob(job);
                }
                if(row[3] != null)
                    empl.setSalary(Double.parseDouble(row[3].toString()));
                if(row[4] != null){
                    long manId = Integer.parseInt(row[4].toString());
                    Men man =  (Men) session.get(Men.class, manId);
                    empl.setMen(man);
                }
                employeesList.add(empl);
            }
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        
        //data model creating:
        EmployeesTableModel model = new EmployeesTableModel(employeesList);
        
        //creating table:
        tblEmployees = new JTable(model);
        
        //setting layout:
        setLayout(new BorderLayout());
        
        //adding table to layout:
        add(new JScrollPane(tblEmployees), BorderLayout.CENTER);
        
         setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    

}
