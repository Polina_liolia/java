package UserInterface;

import hibernatejpamapping.Men;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class MenWindow extends JFrame {

    JTable tblMen;
    List<Men> menList;
    
    public MenWindow() {
        super("Men list");
        InitializeComponent();
    }

    private void InitializeComponent() {
        setSize(400, 400);
        //getting data from DB: 
        SessionFactory mFctory;
        try {
            mFctory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = mFctory.openSession();
        Transaction tx = null;
        menList = new ArrayList<Men>();
        try {
            tx = session.beginTransaction();
            SQLQuery query = session
                    .createSQLQuery("Select FIRST_NAME, LAST_NAME, MIDDLE_NAME, PHONE, BDATE from `MEN`");
            List<Object[]> rows = query.list();
            for (Object[] row : rows) {
                Men men = new Men();
                if(row[0] != null)
                    men.setFirst_name(row[0].toString());
                if(row[1] != null)
                    men.setLast_name(row[1].toString());
                if(row[2] != null)
                    men.setMiddle_name(row[2].toString());
                if(row[3] != null)
                    men.setPhone(row[3].toString());
                if(row[4] != null)
                    men.setBdate(row[4].toString());
                menList.add(men);
            }
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        
        //data model creating:
        MenTableModel model = new MenTableModel(menList);
        
        //creating table:
        tblMen = new JTable(model);
        
        //setting layout:
        setLayout(new BorderLayout());
        
        //adding table to layout:
        add(new JScrollPane(tblMen), BorderLayout.CENTER);
        
         setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
