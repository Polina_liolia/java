package UserInterface;

import hibernatejpamapping.Jobs;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class JobsTableModel extends AbstractTableModel{

    List<Jobs> jobs;
    
    //columns definition:
    String[] columns = new String[]{
        "Id", "Title"
    };
    
    //columns types:
    Class[] columnsTypes = new Class[]{
        Integer.class, String.class
    };

    public JobsTableModel(List<Jobs> jobs) {
        this.jobs = jobs;
    }
   
    
    @Override
    public int getRowCount() {
        return jobs.size();
    }

    @Override
    public int getColumnCount() {
       return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Jobs currentRow = jobs.get(rowIndex);
        return columnIndex == 0 ? currentRow.getId():
                columnIndex == 1 ? currentRow.getJob_title(): null;
    }
    
    @Override
    public String getColumnName(int column) {
        return columns[column];
    }
    
    @Override
    public Class<?> getColumnClass(int column)
    {
        return columnsTypes[column];
    }
}
