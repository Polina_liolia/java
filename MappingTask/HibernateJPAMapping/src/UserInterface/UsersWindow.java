package UserInterface;

import hibernatejpamapping.Men;
import hibernatejpamapping.User;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class UsersWindow extends JFrame{
    JTable tblUsers;
    List<User> usersList;

    public UsersWindow() {
        super("Users list");
        InitializeComponent();
    }
    
    private void InitializeComponent() {
        setSize(400, 400);
        //getting data from DB: 
        SessionFactory mFctory;
        try {
            mFctory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = mFctory.openSession();
        Transaction tx = null;
        usersList = new ArrayList<User>();
        try {
            tx = session.beginTransaction();
            SQLQuery query = session
                    .createSQLQuery("Select USER_ID, FIRST_NAME, LAST_NAME from `User`");
            List<Object[]> rows = query.list();
            for (Object[] row : rows) {
                User user = new User();
                if(row[0] != null)
                   user.setUser_id(Integer.parseInt(row[0].toString()));
                if(row[1] != null)
                   user.setFirst_name(row[1].toString());
                if(row[2] != null)
                   user.setLast_name(row[2].toString());
                
                usersList.add(user);
            }
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        
        //data model creating:
        UserTableModel model = new UserTableModel(usersList);
        
        //creating table:
        tblUsers = new JTable(model);
        
        //setting layout:
        setLayout(new BorderLayout());
        
        //adding table to layout:
        add(new JScrollPane(tblUsers), BorderLayout.CENTER);
        
         setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    

}
