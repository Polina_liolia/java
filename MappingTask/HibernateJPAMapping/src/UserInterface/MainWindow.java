package UserInterface;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class MainWindow extends JFrame {

    JButton btn_Men;
    JButton btn_User;
    JButton btn_Employee;
    JButton btn_Jobs;
    
    public MainWindow() {
        super("Main window");
        InitializeComponents();
    }
    
    private void InitializeComponents()
    {
        setLayout(new FlowLayout());
        //create buttons:
        btn_Men = new JButton("Men");
        btn_Men.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MenWindow menWindow = new MenWindow();
                menWindow.setVisible(true);
            }
        });
        add(btn_Men);
        
        btn_User = new JButton("Users");
        btn_User.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UsersWindow usersWindow = new UsersWindow();
                usersWindow.setVisible(true);
            }
        });
        add(btn_User);
        
        btn_Employee = new JButton("Employee");
        btn_Employee.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EmployeesWindow emplWindow = new EmployeesWindow();
                emplWindow.setVisible(true);
            }
        });
        add(btn_Employee);
        
        btn_Jobs = new JButton("Jobs");
        btn_Jobs.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                JobsWindow jobsWindow = new JobsWindow();
                jobsWindow.setVisible(true);
            }
        });
        add(btn_Jobs);
        
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    

}
