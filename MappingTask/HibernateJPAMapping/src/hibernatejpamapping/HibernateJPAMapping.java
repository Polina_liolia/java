package hibernatejpamapping;

import UserInterface.MainWindow;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class HibernateJPAMapping {

    public static void main(String[] args) {     
      MainWindow mainWindow = new MainWindow();
        mainWindow.setVisible(true);
    }
    
    public static void insertEmployee(){
         SessionFactory mFctory;
        try{
            mFctory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = mFctory.openSession();
        Transaction tx = null;
        Long employeeID = null;
        try{
            tx = session.beginTransaction();
            //create men instance
            Men men = new Men("Karina", "Petrova", "", "-", "07.05.1995");
            Jobs job = new Jobs("manager");
            long menID = (Long) session.save(men);
            //create Employee instance
            Employee employee = new Employee("6", "0678947474", 67876.0, men, job);
            employeeID = (Long) session.save(employee);
            tx.commit();
        }catch (Exception e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    
    
    public static void insertMen(){
         SessionFactory mFctory;
        try{
            mFctory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = mFctory.openSession();
        Transaction tx = null;
        Long menID = null;
        try{
            tx = session.beginTransaction();
            Men men = new Men("Anna", "Nefedova", "", "0503457654", "07.03.1970");
            menID = (Long) session.save(men);
            tx.commit();
        }catch (Exception e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    
     public static void insertUser(){
         SessionFactory mFctory;
        try{
            mFctory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = mFctory.openSession();
        Transaction tx = null;
        Long userID = null;
        try{
            tx = session.beginTransaction();
            User user = new User("Galina", "Ershova");
            userID = (Long) session.save(user);
            tx.commit();
        }catch (Exception e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
     
     public static void insertJobs(){
         SessionFactory mFctory;
        try{
            mFctory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = mFctory.openSession();
        Transaction tx = null;
        Long jobsID = null;
        try{
            tx = session.beginTransaction();
            Jobs jobs = new Jobs("PM");
            jobsID = (Long) session.save(jobs);
            tx.commit();
        }catch (Exception e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
}
