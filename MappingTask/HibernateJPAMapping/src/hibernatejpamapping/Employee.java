package hibernatejpamapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;
    
    @Column(name = "tab_number", nullable = true)
    private String tab_number;
    
    @Column(name = "WORK_PHONE", length = 20)
    private String work_phone;
    
    @Column(name = "id_jobs", nullable = true, insertable = false, updatable = false)
    private long id_jobs;
    
    @ManyToOne
    @JoinColumn(name = "id_jobs")
    private Jobs job;
    
    @Column(name = "SALARY", nullable = true)
    private double salary;
    
    @Column(name = "id_men", nullable = true, insertable = false, updatable = false)
    private long id_men;
    
    @OneToOne
    @JoinColumn(name = "id_men")
    private Men men;

    public Employee() {
    }

    public Employee(String tab_number, String work_phone, double salary, Men men, Jobs job) {
        this.tab_number = tab_number;
        this.work_phone = work_phone;
        this.salary = salary;
        this.men = men;
        this.job = job;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTab_number() {
        return tab_number;
    }

    public void setTab_number(String tab_number) {
        this.tab_number = tab_number;
    }

    public String getWork_phone() {
        return work_phone;
    }

    public void setWork_phone(String work_phone) {
        this.work_phone = work_phone;
    }

    public long getId_jobs() {
        return id_jobs;
    }

    public void setId_jobs(long id_jobs) {
        this.id_jobs = id_jobs;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public long getId_men() {
        return id_men;
    }

    public void setId_men(int id_men) {
        this.id_men = id_men;
    }

    public Men getMen() {
        return men;
    }

    public void setMen(Men men) {
        this.men = men;
    }

    public Jobs getJob() {
        return job;
    }

    public void setJob(Jobs job) {
        this.job = job;
    }
    
    
}
