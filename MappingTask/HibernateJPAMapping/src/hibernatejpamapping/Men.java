package hibernatejpamapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MEN")
public class Men {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    
    @Column(name = "FIRST_NAME", nullable = false, length = 50)
    private String first_name;
    
    @Column(name = "LAST_NAME", nullable = true, length = 50)
    private String last_name;
    
    @Column(name = "MIDDLE_NAME", nullable = true)
    private String middle_name;
    
    @Column(name = "PHONE", nullable = false)
    private String phone;
    
    @Column(name = "BDATE", nullable = true, length = 50)
    private String bdate;

    public Men() {
    }

    public Men(String first_name, String last_name, String middle_name, String phone, String bdate) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.middle_name = middle_name;
        this.phone = phone;
        this.bdate = bdate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBdate() {
        return bdate;
    }

    public void setBdate(String bdate) {
        this.bdate = bdate;
    }
    
    
}
