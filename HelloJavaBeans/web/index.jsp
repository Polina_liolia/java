<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <jsp:useBean id="mybean" scope="session" class="org.itstep.bean.NameHandler"/>
    <jsp:setProperty name="mybean" property="name" />
    <body>
        <h1>Hello, <jsp:getProperty name="mybean" property="name"/></h1>
        <form method="get" name="inputForm" id="inputForm" action="index.jsp">
            Enter your name:
            <input type="text" name="name" id="idname">
            <input type="submit" value="OK">
        </form>
    </body>
</html>
