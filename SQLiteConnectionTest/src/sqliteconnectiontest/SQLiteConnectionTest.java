package sqliteconnectiontest;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SQLiteConnectionTest {

    public static void main(String[] args) {
        try {
            //0 добавить файл драйвера в проект
            //источник драйвера - учетная запись Azure или сайт БД или Maven (желательно)
            //1 - регистрируем класс драйвера (один jar файл может содержать много)
            Class.forName("org.sqlite.JDBC");
            //2 - соединение при помощи connection string (объект класса Connection)
            //внешний вид строки соединения зависит от СУБД
            String connectionString = "jdbc:sqlite:D:\\MyData\\test.db";
            Connection conn = DriverManager.getConnection(connectionString);
            //3 - из Connection получаем или statemant, или collable statement
            if (conn != null) {
                    DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
                    System.out.println("Driver name: " + dm.getDriverName());
                    System.out.println("Driver version: " + dm.getDriverVersion());
                    System.out.println("Product name: " + dm.getDatabaseProductName());
                    System.out.println("Product version: " + dm.getDatabaseProductVersion());
                }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SQLiteConnectionTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SQLiteConnectionTest.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

}
