package simplelambdaexpression;

import com.sun.jmx.remote.internal.ArrayQueue;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.function.*;

public class SimpleLambdaExpression {

    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(-2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        printIntegers(numbers);
        Collections.sort(numbers);
        printIntegers(numbers);

        //different ways to do the same:
        Predicate<Integer> p1 = (Integer num) -> num % 2 == 0;
        printIntegers(numbers, p1); //predicate test

        Predicate<Integer> p2 = (num) -> num % 2 == 0;
        printIntegers(numbers, p2); //predicate test

        Predicate<Integer> p3 = num -> num % 2 == 0;
        printIntegers(numbers, p3); //predicate test

        Predicate<Integer> p4 = num -> num % 2 == 0;
        printIntegers(numbers, p4); //predicate test

        //print all
        printIntegers(numbers, num -> true); //predicate test

        List<String> group = Arrays.asList("Иванов", "Ivanov", "Petrov", "Сидоров");
        Collections.sort(group); //standatr sotr

        //вариант 1: сздать объект класса, для которого реализован интерфейс Comparator (редко используется)
        Collections.sort(group, new MyComparerClass());

        //вариант 2: создат анонимный класс
        Collections.sort(group, new Comparator<String>() {
            @Override
            public int compare(String s, String s1) {
                return s.compareTo(s1);
            }
        });

        //вариант 3: создать лямбда выражение
        Collections.sort(group, (s, s1) -> s.compareTo(s1));

        List<MyUser> users = new ArrayList<>();
        users.add(new MyUser("Иванов", 10));
        users.add(new MyUser("Петров", 10));
        users.add(new MyUser("Ivanov", 10));
        users.add(new MyUser("Сидоров", 10));
        Collections.sort(users, (u1, u2) -> u1.getAge() < u2.getAge() ? -1 : u1.getAge() > u2.getAge() ? 1 : 0);

        numbers.forEach((Integer num) -> {
            System.out.print(num);
            System.out.println();
        });
        
        numbers.forEach(num -> System.out.print(num));
        
        numbers.forEach(System.out::println);
        
        numbers.forEach(SimpleLambdaExpression::isOdd);
    }

    //прямая передача условия
    public static void printIntegers(List<Integer> numbers) {
        for (int num : numbers) {
            if (num % 2 == 0) {
                System.out.printf("%d", num);
            }
        }
    }

    //передача условия через предикат
    public static void printIntegers(List<Integer> numbers, Predicate<Integer> funct) {
        for (int num : numbers) {
            if (funct.test(num)) {
                System.out.printf("%d", num);
            }
        }
    }

    //Предикаты в Java - фукнции, принимающие одно значение и возвращающие boolean
    public static boolean isEven(int num) {
        return num % 2 == 0;
    }

    public static boolean isOdd(int num) {
        return num % 2 != 0;
    }

    public static boolean hasZero(String str) {
        return true;
    }
}

//этот класс может быль только private, т.к. в одном файле может быть только один public class
class MyComparerClass implements Comparator<String> {

    @Override
    public int compare(String s, String s1) {
        return s.compareTo(s1);
    }

}
