/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaconsoletest;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Администратор
 */
public class JavaConsoleTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        //output:
        double mypi = Math.acos(-1);
        System.out.println("Hello world! - привет мир");
        System.out.print(mypi-Math.PI);
        
        //input (old style):
        byte[]bytes = new byte[200];
        int count = System.in.read(bytes);
        double d = Double.parseDouble(new String(bytes));
        System.out.print("double value is "+d);
        
        //input (more convinient):
        Scanner in = new Scanner(System.in);
        int[] nums = new int[5];
        for(int i=0;i < nums.length; i++){
            nums[i]=in.nextInt();
        }

        for(int i=0;i < nums.length; i++){
            System.out.print(nums[i]);
        }
                
        System.out.println();
        
        //reading of command line args:
        if(args.length>0)
        {
            for (int i = 0; i < args.length; i++) 
            {
            String arg = args[i];
            System.out.println(arg);
            }
        }
    }
    
}
